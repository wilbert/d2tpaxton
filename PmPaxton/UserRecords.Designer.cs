namespace Net2_OEM_SDK
{
    partial class UserRecords
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lvwUserRecords = new System.Windows.Forms.ListView ();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader ();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader ();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader ();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader ();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader ();
			this.btnNewUser = new System.Windows.Forms.Button ();
			this.btnEditUser = new System.Windows.Forms.Button ();
			this.btnDeleteUser = new System.Windows.Forms.Button ();
			this.refreshUsersButton = new System.Windows.Forms.Button ();
			this.userIdWhereClauseText = new System.Windows.Forms.TextBox ();
			this.surnameWhereClauseText = new System.Windows.Forms.TextBox ();
			this.userIdFilterButton = new System.Windows.Forms.Button ();
			this.surnameFilterButton = new System.Windows.Forms.Button ();
			this.SuspendLayout ();
			// 
			// lvwUserRecords
			// 
			this.lvwUserRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lvwUserRecords.Columns.AddRange (new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
			this.lvwUserRecords.FullRowSelect = true;
			this.lvwUserRecords.GridLines = true;
			this.lvwUserRecords.Location = new System.Drawing.Point (3, 3);
			this.lvwUserRecords.MultiSelect = false;
			this.lvwUserRecords.Name = "lvwUserRecords";
			this.lvwUserRecords.Size = new System.Drawing.Size (643, 320);
			this.lvwUserRecords.TabIndex = 28;
			this.lvwUserRecords.UseCompatibleStateImageBehavior = false;
			this.lvwUserRecords.View = System.Windows.Forms.View.Details;
			this.lvwUserRecords.DoubleClick += new System.EventHandler (this.lvwUserRecords_DoubleClick);
			this.lvwUserRecords.SelectedIndexChanged += new System.EventHandler (this.lvwUserRecords_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "User ID";
			this.columnHeader1.Width = 62;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "First name";
			this.columnHeader2.Width = 100;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Surname";
			this.columnHeader3.Width = 100;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Department";
			this.columnHeader4.Width = 111;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Access level";
			this.columnHeader5.Width = 142;
			// 
			// btnNewUser
			// 
			this.btnNewUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNewUser.Location = new System.Drawing.Point (571, 379);
			this.btnNewUser.Name = "btnNewUser";
			this.btnNewUser.Size = new System.Drawing.Size (75, 23);
			this.btnNewUser.TabIndex = 29;
			this.btnNewUser.Text = "New user";
			this.btnNewUser.UseVisualStyleBackColor = true;
			this.btnNewUser.Click += new System.EventHandler (this.btnNewUser_Click);
			// 
			// btnEditUser
			// 
			this.btnEditUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEditUser.Location = new System.Drawing.Point (490, 379);
			this.btnEditUser.Name = "btnEditUser";
			this.btnEditUser.Size = new System.Drawing.Size (75, 23);
			this.btnEditUser.TabIndex = 29;
			this.btnEditUser.Text = "Edit user";
			this.btnEditUser.UseVisualStyleBackColor = true;
			this.btnEditUser.Click += new System.EventHandler (this.btnEditUser_Click);
			// 
			// btnDeleteUser
			// 
			this.btnDeleteUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDeleteUser.Location = new System.Drawing.Point (409, 379);
			this.btnDeleteUser.Name = "btnDeleteUser";
			this.btnDeleteUser.Size = new System.Drawing.Size (75, 23);
			this.btnDeleteUser.TabIndex = 29;
			this.btnDeleteUser.Text = "Delete user";
			this.btnDeleteUser.UseVisualStyleBackColor = true;
			this.btnDeleteUser.Click += new System.EventHandler (this.btnDeleteUser_Click);
			// 
			// refreshUsersButton
			// 
			this.refreshUsersButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.refreshUsersButton.Location = new System.Drawing.Point (3, 377);
			this.refreshUsersButton.Name = "refreshUsersButton";
			this.refreshUsersButton.Size = new System.Drawing.Size (75, 23);
			this.refreshUsersButton.TabIndex = 30;
			this.refreshUsersButton.Text = "Refresh";
			this.refreshUsersButton.UseVisualStyleBackColor = true;
			this.refreshUsersButton.Click += new System.EventHandler (this.refreshUsersButton_Click);
			// 
			// userIdWhereClauseText
			// 
			this.userIdWhereClauseText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.userIdWhereClauseText.Location = new System.Drawing.Point (3, 328);
			this.userIdWhereClauseText.Name = "userIdWhereClauseText";
			this.userIdWhereClauseText.ReadOnly = true;
			this.userIdWhereClauseText.Size = new System.Drawing.Size (237, 20);
			this.userIdWhereClauseText.TabIndex = 32;
			// 
			// surnameWhereClauseText
			// 
			this.surnameWhereClauseText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.surnameWhereClauseText.Location = new System.Drawing.Point (4, 351);
			this.surnameWhereClauseText.Name = "surnameWhereClauseText";
			this.surnameWhereClauseText.ReadOnly = true;
			this.surnameWhereClauseText.Size = new System.Drawing.Size (236, 20);
			this.surnameWhereClauseText.TabIndex = 32;
			// 
			// userIdFilterButton
			// 
			this.userIdFilterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.userIdFilterButton.Location = new System.Drawing.Point (84, 377);
			this.userIdFilterButton.Name = "userIdFilterButton";
			this.userIdFilterButton.Size = new System.Drawing.Size (75, 23);
			this.userIdFilterButton.TabIndex = 33;
			this.userIdFilterButton.Text = "By User Id";
			this.userIdFilterButton.UseVisualStyleBackColor = true;
			this.userIdFilterButton.Click += new System.EventHandler (this.userIdFilterButton_Click);
			// 
			// surnameFilterButton
			// 
			this.surnameFilterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.surnameFilterButton.Location = new System.Drawing.Point (165, 377);
			this.surnameFilterButton.Name = "surnameFilterButton";
			this.surnameFilterButton.Size = new System.Drawing.Size (75, 23);
			this.surnameFilterButton.TabIndex = 33;
			this.surnameFilterButton.Text = "By Surname";
			this.surnameFilterButton.UseVisualStyleBackColor = true;
			this.surnameFilterButton.Click += new System.EventHandler (this.surnameFilterButton_Click);
			// 
			// UserRecords
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add (this.surnameFilterButton);
			this.Controls.Add (this.userIdFilterButton);
			this.Controls.Add (this.surnameWhereClauseText);
			this.Controls.Add (this.userIdWhereClauseText);
			this.Controls.Add (this.refreshUsersButton);
			this.Controls.Add (this.btnDeleteUser);
			this.Controls.Add (this.btnEditUser);
			this.Controls.Add (this.btnNewUser);
			this.Controls.Add (this.lvwUserRecords);
			this.Name = "UserRecords";
			this.Size = new System.Drawing.Size (649, 405);
			this.ResumeLayout (false);
			this.PerformLayout ();

        }

        #endregion

        private System.Windows.Forms.ListView lvwUserRecords;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnNewUser;
        private System.Windows.Forms.Button btnEditUser;
        private System.Windows.Forms.Button btnDeleteUser;
		private System.Windows.Forms.Button refreshUsersButton;
		private System.Windows.Forms.TextBox userIdWhereClauseText;
		private System.Windows.Forms.TextBox surnameWhereClauseText;
		private System.Windows.Forms.Button userIdFilterButton;
		private System.Windows.Forms.Button surnameFilterButton;
    }
}
