using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
    public partial class FormMain : Form
    {
        private OemClient _net2Client;

        public FormMain(OemClient net2Client)
        {
            InitializeComponent();
            _net2Client = net2Client;
            EventsControl.Initialise(_net2Client);
            OpenDoorControl.Initialise(_net2Client);
            UserRecordsControl.Initialise(_net2Client);

            FormClosed += new FormClosedEventHandler(FormMain_FormClosed);
        }

        void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            OpenDoorControl.Close();
            
            if (_net2Client != null)
            {
                _net2Client.Dispose();
                _net2Client = null;
            }

            Dispose();

            if (Application.AllowQuit)
            {
                Application.Exit();
            }
        }
    }
}