namespace Net2_OEM_SDK
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabOptions = new System.Windows.Forms.TabControl();
            this.tabOpenDoor = new System.Windows.Forms.TabPage();
            this.tabUserRecords = new System.Windows.Forms.TabPage();
            this.Events = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.OpenDoorControl = new Net2_OEM_SDK.OpenDoor();
            this.UserRecordsControl = new Net2_OEM_SDK.UserRecords();
            this.EventsControl = new Net2_OEM_SDK.Events();
            this.tabOptions.SuspendLayout();
            this.tabOpenDoor.SuspendLayout();
            this.tabUserRecords.SuspendLayout();
            this.Events.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabOptions
            // 
            this.tabOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabOptions.Controls.Add(this.tabOpenDoor);
            this.tabOptions.Controls.Add(this.tabUserRecords);
            this.tabOptions.Controls.Add(this.Events);
            this.tabOptions.Location = new System.Drawing.Point(0, 0);
            this.tabOptions.Name = "tabOptions";
            this.tabOptions.SelectedIndex = 0;
            this.tabOptions.Size = new System.Drawing.Size(677, 478);
            this.tabOptions.TabIndex = 0;
            // 
            // tabOpenDoor
            // 
            this.tabOpenDoor.Controls.Add(this.OpenDoorControl);
            this.tabOpenDoor.Location = new System.Drawing.Point(4, 22);
            this.tabOpenDoor.Name = "tabOpenDoor";
            this.tabOpenDoor.Padding = new System.Windows.Forms.Padding(3);
            this.tabOpenDoor.Size = new System.Drawing.Size(681, 452);
            this.tabOpenDoor.TabIndex = 0;
            this.tabOpenDoor.Text = "Open door";
            this.tabOpenDoor.UseVisualStyleBackColor = true;
            // 
            // tabUserRecords
            // 
            this.tabUserRecords.Controls.Add(this.UserRecordsControl);
            this.tabUserRecords.Location = new System.Drawing.Point(4, 22);
            this.tabUserRecords.Name = "tabUserRecords";
            this.tabUserRecords.Padding = new System.Windows.Forms.Padding(3);
            this.tabUserRecords.Size = new System.Drawing.Size(681, 452);
            this.tabUserRecords.TabIndex = 1;
            this.tabUserRecords.Text = "User records";
            this.tabUserRecords.UseVisualStyleBackColor = true;
            // 
            // Events
            // 
            this.Events.Controls.Add(this.EventsControl);
            this.Events.Location = new System.Drawing.Point(4, 22);
            this.Events.Name = "Events";
            this.Events.Size = new System.Drawing.Size(669, 452);
            this.Events.TabIndex = 2;
            this.Events.Text = "Events";
            this.Events.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 481);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(689, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // OpenDoorControl
            // 
            this.OpenDoorControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OpenDoorControl.Location = new System.Drawing.Point(3, 3);
            this.OpenDoorControl.MinimumSize = new System.Drawing.Size(521, 445);
            this.OpenDoorControl.Name = "OpenDoorControl";
            this.OpenDoorControl.Size = new System.Drawing.Size(675, 446);
            this.OpenDoorControl.TabIndex = 0;
            // 
            // UserRecordsControl
            // 
            this.UserRecordsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserRecordsControl.Location = new System.Drawing.Point(3, 3);
            this.UserRecordsControl.Name = "UserRecordsControl";
            this.UserRecordsControl.Size = new System.Drawing.Size(675, 446);
            this.UserRecordsControl.TabIndex = 0;
            // 
            // EventsControl
            // 
            this.EventsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EventsControl.Location = new System.Drawing.Point(0, 0);
            this.EventsControl.Name = "EventsControl";
            this.EventsControl.Size = new System.Drawing.Size(669, 452);
            this.EventsControl.TabIndex = 1;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 503);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabOptions);
            this.MinimumSize = new System.Drawing.Size(697, 537);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.tabOptions.ResumeLayout(false);
            this.tabOpenDoor.ResumeLayout(false);
            this.tabUserRecords.ResumeLayout(false);
            this.Events.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabOptions;
        private System.Windows.Forms.TabPage tabOpenDoor;
        private System.Windows.Forms.TabPage tabUserRecords;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private OpenDoor OpenDoorControl;
        private UserRecords UserRecordsControl;
        private new System.Windows.Forms.TabPage Events;
        private Events EventsControl;
    }
}