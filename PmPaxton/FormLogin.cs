using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
    public partial class FormLogin : Form
    {
        private const int REMOTE_PORT = 8025;
        private const string REMOTE_HOST = "192.168.210.125";
		private OemClient _net2Client;
		private FormMain _mainForm;
        private string _remoteHost;
		
        public FormLogin()
        {
            InitializeComponent();
            			
            txtServer.Text = REMOTE_HOST;
        }

		/// <summary>
		/// Instantiate the client library and authenticate the user.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
		    try
			{
                _remoteHost = txtServer.Text;

			    CreateClient();

                if (AuthenticateUser(txtUserName.Text, txtPassword.Text))
                {
                    _mainForm = new FormMain(_net2Client);
                    _mainForm.Activate();
                    _mainForm.Visible = true;

                    Visible = false;
                    return;
                }
                
			    loginStatusLabel.Text = "User name not found.";
			}
			catch (Exception excp)
			{
				loginStatusLabel.Text = excp.Message;
			}
        }
        
        private bool AuthenticateUser(string userName, string password)
        {
            IOperators operators = _net2Client.GetListOfOperators();
            Dictionary<int, string> operatorsList = operators.UsersDictionary();
            foreach (int userID in operatorsList.Keys)
            {
                if (operatorsList[userID] == userName)
                {
                    Dictionary<string, int> methodList = _net2Client.AuthenticateUser(userID, password);
                    return (methodList != null);
                }
            }
            return false;
        } 


		private void CreateClient ()
		{
			_net2Client = new OemClient (_remoteHost, REMOTE_PORT);

			if (_net2Client.LastErrorMessage != null)
			{
				loginStatusLabel.Text = _net2Client.LastErrorMessage;
				return;
			}
		}

		private void FormLogin_FormClosing (object sender, FormClosingEventArgs e)
		{
			if (_net2Client != null)
			{
				_net2Client.Dispose ();
				_net2Client = null;
			}
		}

        private void FormLogin_Activated(object sender, EventArgs e)
        {
            btnConnect.Focus();
        }
    }
}