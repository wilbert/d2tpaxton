using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
	partial class UserRecord
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
            this.activeCheckBox = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.addCardButton = new System.Windows.Forms.Button();
            this.cardNumberLabel = new System.Windows.Forms.Label();
            this.cardNumberTextBox = new System.Windows.Forms.TextBox();
            this.pictureFileTextBox = new System.Windows.Forms.TextBox();
            this.generatePinButton = new System.Windows.Forms.Button();
            this.pinTextBox = new System.Windows.Forms.TextBox();
            this.alarmUserCheckBox = new System.Windows.Forms.CheckBox();
            this.antiPassbackCheckBox = new System.Windows.Forms.CheckBox();
            this.accessLevelComboBox = new System.Windows.Forms.ComboBox();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.extensionLabel = new System.Windows.Forms.Label();
            this.pictureFileLabel = new System.Windows.Forms.Label();
            this.telephoneLabel = new System.Windows.Forms.Label();
            this.accessLevelLabel = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.middleNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.middleNameTextBox = new System.Windows.Forms.TextBox();
            this.telephoneExtTextBox = new System.Windows.Forms.TextBox();
            this.telephoneNoTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.newUserStatusStrip = new System.Windows.Forms.StatusStrip();
            this.newUserStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deleteCardButton = new System.Windows.Forms.Button();
            this.cardsComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.newUserTabControl = new System.Windows.Forms.TabControl();
            this.userTabPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.expiresCheckBox = new System.Windows.Forms.CheckBox();
            this.expiryDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.activationDate = new System.Windows.Forms.DateTimePicker();
            this.faxTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accessLevelTabPage = new System.Windows.Forms.TabPage();
            this.readerAreasGridView = new System.Windows.Forms.DataGridView();
            this.individualRadioButton = new System.Windows.Forms.RadioButton();
            this.accessLevelRadioButton = new System.Windows.Forms.RadioButton();
            this.customFieldsTabPage = new System.Windows.Forms.TabPage();
            this.field14Label = new System.Windows.Forms.Label();
            this.field7Label = new System.Windows.Forms.Label();
            this.field6Label = new System.Windows.Forms.Label();
            this.field11Label = new System.Windows.Forms.Label();
            this.field4Label = new System.Windows.Forms.Label();
            this.field9Label = new System.Windows.Forms.Label();
            this.field2Label = new System.Windows.Forms.Label();
            this.field12Label = new System.Windows.Forms.Label();
            this.field5Label = new System.Windows.Forms.Label();
            this.field10Label = new System.Windows.Forms.Label();
            this.field3Label = new System.Windows.Forms.Label();
            this.field8Label = new System.Windows.Forms.Label();
            this.field1Label = new System.Windows.Forms.Label();
            this.field14TextBox = new System.Windows.Forms.TextBox();
            this.field7TextBox = new System.Windows.Forms.TextBox();
            this.field6TextBox = new System.Windows.Forms.TextBox();
            this.field12TextBox = new System.Windows.Forms.TextBox();
            this.field5TextBox = new System.Windows.Forms.TextBox();
            this.field11TextBox = new System.Windows.Forms.TextBox();
            this.field10TextBox = new System.Windows.Forms.TextBox();
            this.field4TextBox = new System.Windows.Forms.TextBox();
            this.field9TextBox = new System.Windows.Forms.TextBox();
            this.field3TextBox = new System.Windows.Forms.TextBox();
            this.field8TextBox = new System.Windows.Forms.TextBox();
            this.field2TextBox = new System.Windows.Forms.TextBox();
            this.field1TextBox = new System.Windows.Forms.TextBox();
            this.memoTabPage = new System.Windows.Forms.TabPage();
            this.field13Label = new System.Windows.Forms.Label();
            this.field13TextBox = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtPinLength = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.newUserStatusStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.newUserTabControl.SuspendLayout();
            this.userTabPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.accessLevelTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readerAreasGridView)).BeginInit();
            this.customFieldsTabPage.SuspendLayout();
            this.memoTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // activeCheckBox
            // 
            this.activeCheckBox.AutoSize = true;
            this.activeCheckBox.Checked = true;
            this.activeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.activeCheckBox.Location = new System.Drawing.Point(371, 148);
            this.activeCheckBox.Name = "activeCheckBox";
            this.activeCheckBox.Size = new System.Drawing.Size(56, 17);
            this.activeCheckBox.TabIndex = 19;
            this.activeCheckBox.Text = "Active";
            this.activeCheckBox.UseVisualStyleBackColor = true;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Location = new System.Drawing.Point(619, 12);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 18;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.updateUserButton_Click);
            // 
            // addCardButton
            // 
            this.addCardButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addCardButton.Location = new System.Drawing.Point(516, 26);
            this.addCardButton.Name = "addCardButton";
            this.addCardButton.Size = new System.Drawing.Size(75, 23);
            this.addCardButton.TabIndex = 17;
            this.addCardButton.Text = "Add Card";
            this.addCardButton.UseVisualStyleBackColor = true;
            this.addCardButton.Click += new System.EventHandler(this.addCardButton_Click);
            // 
            // cardNumberLabel
            // 
            this.cardNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cardNumberLabel.AutoSize = true;
            this.cardNumberLabel.Location = new System.Drawing.Point(61, 31);
            this.cardNumberLabel.Name = "cardNumberLabel";
            this.cardNumberLabel.Size = new System.Drawing.Size(52, 13);
            this.cardNumberLabel.TabIndex = 14;
            this.cardNumberLabel.Text = "Card No.:";
            // 
            // cardNumberTextBox
            // 
            this.cardNumberTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cardNumberTextBox.Location = new System.Drawing.Point(119, 28);
            this.cardNumberTextBox.MaxLength = 8;
            this.cardNumberTextBox.Name = "cardNumberTextBox";
            this.cardNumberTextBox.Size = new System.Drawing.Size(391, 20);
            this.cardNumberTextBox.TabIndex = 13;
            this.cardNumberTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cardNumberTextBox_KeyPress);
            // 
            // pictureFileTextBox
            // 
            this.pictureFileTextBox.Location = new System.Drawing.Point(144, 172);
            this.pictureFileTextBox.Name = "pictureFileTextBox";
            this.pictureFileTextBox.Size = new System.Drawing.Size(207, 20);
            this.pictureFileTextBox.TabIndex = 7;
            // 
            // generatePinButton
            // 
            this.generatePinButton.Enabled = false;
            this.generatePinButton.Location = new System.Drawing.Point(276, 196);
            this.generatePinButton.Name = "generatePinButton";
            this.generatePinButton.Size = new System.Drawing.Size(75, 23);
            this.generatePinButton.TabIndex = 11;
            this.generatePinButton.Text = "Auto PIN";
            this.generatePinButton.UseVisualStyleBackColor = true;
            this.generatePinButton.Visible = false;
            this.generatePinButton.Click += new System.EventHandler(this.generatePinButton_Click);
            // 
            // pinTextBox
            // 
            this.pinTextBox.Location = new System.Drawing.Point(144, 198);
            this.pinTextBox.MaxLength = 4;
            this.pinTextBox.Name = "pinTextBox";
            this.pinTextBox.Size = new System.Drawing.Size(126, 20);
            this.pinTextBox.TabIndex = 10;
            // 
            // alarmUserCheckBox
            // 
            this.alarmUserCheckBox.AutoSize = true;
            this.alarmUserCheckBox.Location = new System.Drawing.Point(371, 200);
            this.alarmUserCheckBox.Name = "alarmUserCheckBox";
            this.alarmUserCheckBox.Size = new System.Drawing.Size(77, 17);
            this.alarmUserCheckBox.TabIndex = 9;
            this.alarmUserCheckBox.Text = "Alarm User";
            this.alarmUserCheckBox.UseVisualStyleBackColor = true;
            // 
            // antiPassbackCheckBox
            // 
            this.antiPassbackCheckBox.AutoSize = true;
            this.antiPassbackCheckBox.Location = new System.Drawing.Point(371, 174);
            this.antiPassbackCheckBox.Name = "antiPassbackCheckBox";
            this.antiPassbackCheckBox.Size = new System.Drawing.Size(94, 17);
            this.antiPassbackCheckBox.TabIndex = 8;
            this.antiPassbackCheckBox.Text = "Anti-Passback";
            this.antiPassbackCheckBox.UseVisualStyleBackColor = true;
            // 
            // accessLevelComboBox
            // 
            this.accessLevelComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.accessLevelComboBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.accessLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accessLevelComboBox.FormattingEnabled = true;
            this.accessLevelComboBox.Location = new System.Drawing.Point(133, 16);
            this.accessLevelComboBox.Name = "accessLevelComboBox";
            this.accessLevelComboBox.Size = new System.Drawing.Size(324, 21);
            this.accessLevelComboBox.TabIndex = 4;
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Location = new System.Drawing.Point(144, 93);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(207, 21);
            this.departmentComboBox.TabIndex = 3;
            // 
            // extensionLabel
            // 
            this.extensionLabel.AutoSize = true;
            this.extensionLabel.Location = new System.Drawing.Point(251, 123);
            this.extensionLabel.Name = "extensionLabel";
            this.extensionLabel.Size = new System.Drawing.Size(28, 13);
            this.extensionLabel.TabIndex = 1;
            this.extensionLabel.Text = "Ext.:";
            this.extensionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureFileLabel
            // 
            this.pictureFileLabel.AutoSize = true;
            this.pictureFileLabel.Location = new System.Drawing.Point(13, 175);
            this.pictureFileLabel.Name = "pictureFileLabel";
            this.pictureFileLabel.Size = new System.Drawing.Size(93, 13);
            this.pictureFileLabel.TabIndex = 1;
            this.pictureFileLabel.Text = "Picture File Name:";
            this.pictureFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // telephoneLabel
            // 
            this.telephoneLabel.AutoSize = true;
            this.telephoneLabel.Location = new System.Drawing.Point(13, 123);
            this.telephoneLabel.Name = "telephoneLabel";
            this.telephoneLabel.Size = new System.Drawing.Size(61, 13);
            this.telephoneLabel.TabIndex = 1;
            this.telephoneLabel.Text = "Telephone:";
            this.telephoneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // accessLevelLabel
            // 
            this.accessLevelLabel.AutoSize = true;
            this.accessLevelLabel.Location = new System.Drawing.Point(53, 19);
            this.accessLevelLabel.Name = "accessLevelLabel";
            this.accessLevelLabel.Size = new System.Drawing.Size(74, 13);
            this.accessLevelLabel.TabIndex = 1;
            this.accessLevelLabel.Text = "Access Level:";
            this.accessLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(13, 96);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(65, 13);
            this.departmentLabel.TabIndex = 1;
            this.departmentLabel.Text = "Deptarment:";
            this.departmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(13, 70);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(52, 13);
            this.surnameLabel.TabIndex = 1;
            this.surnameLabel.Text = "Surname:";
            this.surnameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // middleNameLabel
            // 
            this.middleNameLabel.AutoSize = true;
            this.middleNameLabel.Location = new System.Drawing.Point(13, 44);
            this.middleNameLabel.Name = "middleNameLabel";
            this.middleNameLabel.Size = new System.Drawing.Size(72, 13);
            this.middleNameLabel.TabIndex = 1;
            this.middleNameLabel.Text = "Middle Name:";
            this.middleNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(13, 18);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(60, 13);
            this.firstNameLabel.TabIndex = 1;
            this.firstNameLabel.Text = "First Name:";
            this.firstNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // middleNameTextBox
            // 
            this.middleNameTextBox.Location = new System.Drawing.Point(144, 41);
            this.middleNameTextBox.MaxLength = 50;
            this.middleNameTextBox.Name = "middleNameTextBox";
            this.middleNameTextBox.Size = new System.Drawing.Size(207, 20);
            this.middleNameTextBox.TabIndex = 1;
            // 
            // telephoneExtTextBox
            // 
            this.telephoneExtTextBox.Location = new System.Drawing.Point(276, 120);
            this.telephoneExtTextBox.MaxLength = 10;
            this.telephoneExtTextBox.Name = "telephoneExtTextBox";
            this.telephoneExtTextBox.Size = new System.Drawing.Size(75, 20);
            this.telephoneExtTextBox.TabIndex = 6;
            // 
            // telephoneNoTextBox
            // 
            this.telephoneNoTextBox.Location = new System.Drawing.Point(144, 120);
            this.telephoneNoTextBox.MaxLength = 30;
            this.telephoneNoTextBox.Name = "telephoneNoTextBox";
            this.telephoneNoTextBox.Size = new System.Drawing.Size(101, 20);
            this.telephoneNoTextBox.TabIndex = 5;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(144, 67);
            this.surnameTextBox.MaxLength = 50;
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(207, 20);
            this.surnameTextBox.TabIndex = 2;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(144, 15);
            this.firstNameTextBox.MaxLength = 50;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(207, 20);
            this.firstNameTextBox.TabIndex = 0;
            // 
            // newUserStatusStrip
            // 
            this.newUserStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserStatusLabel});
            this.newUserStatusStrip.Location = new System.Drawing.Point(0, 384);
            this.newUserStatusStrip.Name = "newUserStatusStrip";
            this.newUserStatusStrip.Size = new System.Drawing.Size(706, 22);
            this.newUserStatusStrip.TabIndex = 1;
            this.newUserStatusStrip.Text = "statusStrip1";
            // 
            // newUserStatusLabel
            // 
            this.newUserStatusLabel.Name = "newUserStatusLabel";
            this.newUserStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this.newUserStatusLabel.Size = new System.Drawing.Size(691, 17);
            this.newUserStatusLabel.Spring = true;
            this.newUserStatusLabel.Text = "Ready.";
            this.newUserStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.deleteCardButton);
            this.groupBox1.Controls.Add(this.cardsComboBox);
            this.groupBox1.Controls.Add(this.addCardButton);
            this.groupBox1.Controls.Add(this.cardNumberTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cardNumberLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 293);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(597, 81);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Card Administration";
            // 
            // deleteCardButton
            // 
            this.deleteCardButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteCardButton.Location = new System.Drawing.Point(516, 52);
            this.deleteCardButton.Name = "deleteCardButton";
            this.deleteCardButton.Size = new System.Drawing.Size(75, 23);
            this.deleteCardButton.TabIndex = 1;
            this.deleteCardButton.Text = "Delete";
            this.deleteCardButton.UseVisualStyleBackColor = true;
            this.deleteCardButton.Click += new System.EventHandler(this.deleteCardButton_Click);
            // 
            // cardsComboBox
            // 
            this.cardsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cardsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cardsComboBox.FormattingEnabled = true;
            this.cardsComboBox.Location = new System.Drawing.Point(119, 54);
            this.cardsComboBox.Name = "cardsComboBox";
            this.cardsComboBox.Size = new System.Drawing.Size(391, 21);
            this.cardsComboBox.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "User\'s cards:";
            // 
            // newUserTabControl
            // 
            this.newUserTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.newUserTabControl.Controls.Add(this.userTabPage);
            this.newUserTabControl.Controls.Add(this.accessLevelTabPage);
            this.newUserTabControl.Controls.Add(this.customFieldsTabPage);
            this.newUserTabControl.Controls.Add(this.memoTabPage);
            this.newUserTabControl.Location = new System.Drawing.Point(12, 12);
            this.newUserTabControl.Name = "newUserTabControl";
            this.newUserTabControl.SelectedIndex = 0;
            this.newUserTabControl.Size = new System.Drawing.Size(601, 275);
            this.newUserTabControl.TabIndex = 3;
            // 
            // userTabPage
            // 
            this.userTabPage.Controls.Add(this.txtPinLength);
            this.userTabPage.Controls.Add(this.groupBox3);
            this.userTabPage.Controls.Add(this.groupBox2);
            this.userTabPage.Controls.Add(this.faxTextBox);
            this.userTabPage.Controls.Add(this.activeCheckBox);
            this.userTabPage.Controls.Add(this.firstNameTextBox);
            this.userTabPage.Controls.Add(this.surnameTextBox);
            this.userTabPage.Controls.Add(this.telephoneNoTextBox);
            this.userTabPage.Controls.Add(this.telephoneExtTextBox);
            this.userTabPage.Controls.Add(this.middleNameTextBox);
            this.userTabPage.Controls.Add(this.firstNameLabel);
            this.userTabPage.Controls.Add(this.pictureFileTextBox);
            this.userTabPage.Controls.Add(this.middleNameLabel);
            this.userTabPage.Controls.Add(this.surnameLabel);
            this.userTabPage.Controls.Add(this.generatePinButton);
            this.userTabPage.Controls.Add(this.departmentLabel);
            this.userTabPage.Controls.Add(this.pinTextBox);
            this.userTabPage.Controls.Add(this.telephoneLabel);
            this.userTabPage.Controls.Add(this.alarmUserCheckBox);
            this.userTabPage.Controls.Add(this.label1);
            this.userTabPage.Controls.Add(this.label4);
            this.userTabPage.Controls.Add(this.label2);
            this.userTabPage.Controls.Add(this.pictureFileLabel);
            this.userTabPage.Controls.Add(this.antiPassbackCheckBox);
            this.userTabPage.Controls.Add(this.extensionLabel);
            this.userTabPage.Controls.Add(this.departmentComboBox);
            this.userTabPage.Location = new System.Drawing.Point(4, 22);
            this.userTabPage.Name = "userTabPage";
            this.userTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.userTabPage.Size = new System.Drawing.Size(593, 249);
            this.userTabPage.TabIndex = 0;
            this.userTabPage.Text = "User";
            this.userTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.expiresCheckBox);
            this.groupBox3.Controls.Add(this.expiryDate);
            this.groupBox3.Location = new System.Drawing.Point(362, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(214, 57);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            // 
            // expiresCheckBox
            // 
            this.expiresCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.expiresCheckBox.AutoSize = true;
            this.expiresCheckBox.Location = new System.Drawing.Point(9, 0);
            this.expiresCheckBox.Name = "expiresCheckBox";
            this.expiresCheckBox.Size = new System.Drawing.Size(60, 17);
            this.expiresCheckBox.TabIndex = 9;
            this.expiresCheckBox.Text = "Expires";
            this.expiresCheckBox.UseVisualStyleBackColor = true;
            this.expiresCheckBox.CheckStateChanged += new System.EventHandler(this.expiresCheckBox_CheckStateChanged);
            // 
            // expiryDate
            // 
            this.expiryDate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.expiryDate.Enabled = false;
            this.expiryDate.Location = new System.Drawing.Point(36, 23);
            this.expiryDate.Name = "expiryDate";
            this.expiryDate.Size = new System.Drawing.Size(172, 20);
            this.expiryDate.TabIndex = 21;
            this.expiryDate.Value = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.activationDate);
            this.groupBox2.Location = new System.Drawing.Point(362, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 58);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Activation date";
            // 
            // activationDate
            // 
            this.activationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.activationDate.Location = new System.Drawing.Point(36, 25);
            this.activationDate.Name = "activationDate";
            this.activationDate.Size = new System.Drawing.Size(172, 20);
            this.activationDate.TabIndex = 21;
            this.activationDate.Value = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            // 
            // faxTextBox
            // 
            this.faxTextBox.Location = new System.Drawing.Point(144, 146);
            this.faxTextBox.Name = "faxTextBox";
            this.faxTextBox.Size = new System.Drawing.Size(207, 20);
            this.faxTextBox.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fax No:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "PIN:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // accessLevelTabPage
            // 
            this.accessLevelTabPage.Controls.Add(this.readerAreasGridView);
            this.accessLevelTabPage.Controls.Add(this.individualRadioButton);
            this.accessLevelTabPage.Controls.Add(this.accessLevelRadioButton);
            this.accessLevelTabPage.Controls.Add(this.accessLevelComboBox);
            this.accessLevelTabPage.Controls.Add(this.accessLevelLabel);
            this.accessLevelTabPage.Location = new System.Drawing.Point(4, 22);
            this.accessLevelTabPage.Name = "accessLevelTabPage";
            this.accessLevelTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.accessLevelTabPage.Size = new System.Drawing.Size(593, 249);
            this.accessLevelTabPage.TabIndex = 1;
            this.accessLevelTabPage.Text = "Access";
            this.accessLevelTabPage.UseVisualStyleBackColor = true;
            // 
            // readerAreasGridView
            // 
            this.readerAreasGridView.AllowUserToAddRows = false;
            this.readerAreasGridView.AllowUserToDeleteRows = false;
            this.readerAreasGridView.AllowUserToOrderColumns = true;
            this.readerAreasGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.readerAreasGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.readerAreasGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.readerAreasGridView.Location = new System.Drawing.Point(9, 52);
            this.readerAreasGridView.MultiSelect = false;
            this.readerAreasGridView.Name = "readerAreasGridView";
            this.readerAreasGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.readerAreasGridView.Size = new System.Drawing.Size(578, 191);
            this.readerAreasGridView.TabIndex = 6;
            // 
            // individualRadioButton
            // 
            this.individualRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.individualRadioButton.AutoSize = true;
            this.individualRadioButton.Location = new System.Drawing.Point(498, 29);
            this.individualRadioButton.Name = "individualRadioButton";
            this.individualRadioButton.Size = new System.Drawing.Size(70, 17);
            this.individualRadioButton.TabIndex = 5;
            this.individualRadioButton.Text = "Individual";
            this.individualRadioButton.UseVisualStyleBackColor = true;
            this.individualRadioButton.CheckedChanged += new System.EventHandler(this.individualRadioButton_CheckedChanged);
            // 
            // accessLevelRadioButton
            // 
            this.accessLevelRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accessLevelRadioButton.AutoSize = true;
            this.accessLevelRadioButton.Checked = true;
            this.accessLevelRadioButton.Location = new System.Drawing.Point(498, 6);
            this.accessLevelRadioButton.Name = "accessLevelRadioButton";
            this.accessLevelRadioButton.Size = new System.Drawing.Size(89, 17);
            this.accessLevelRadioButton.TabIndex = 5;
            this.accessLevelRadioButton.TabStop = true;
            this.accessLevelRadioButton.Text = "Access Level";
            this.accessLevelRadioButton.UseVisualStyleBackColor = true;
            this.accessLevelRadioButton.CheckedChanged += new System.EventHandler(this.accessLevelRadioButton_CheckedChanged);
            // 
            // customFieldsTabPage
            // 
            this.customFieldsTabPage.Controls.Add(this.field14Label);
            this.customFieldsTabPage.Controls.Add(this.field7Label);
            this.customFieldsTabPage.Controls.Add(this.field6Label);
            this.customFieldsTabPage.Controls.Add(this.field11Label);
            this.customFieldsTabPage.Controls.Add(this.field4Label);
            this.customFieldsTabPage.Controls.Add(this.field9Label);
            this.customFieldsTabPage.Controls.Add(this.field2Label);
            this.customFieldsTabPage.Controls.Add(this.field12Label);
            this.customFieldsTabPage.Controls.Add(this.field5Label);
            this.customFieldsTabPage.Controls.Add(this.field10Label);
            this.customFieldsTabPage.Controls.Add(this.field3Label);
            this.customFieldsTabPage.Controls.Add(this.field8Label);
            this.customFieldsTabPage.Controls.Add(this.field1Label);
            this.customFieldsTabPage.Controls.Add(this.field14TextBox);
            this.customFieldsTabPage.Controls.Add(this.field7TextBox);
            this.customFieldsTabPage.Controls.Add(this.field6TextBox);
            this.customFieldsTabPage.Controls.Add(this.field12TextBox);
            this.customFieldsTabPage.Controls.Add(this.field5TextBox);
            this.customFieldsTabPage.Controls.Add(this.field11TextBox);
            this.customFieldsTabPage.Controls.Add(this.field10TextBox);
            this.customFieldsTabPage.Controls.Add(this.field4TextBox);
            this.customFieldsTabPage.Controls.Add(this.field9TextBox);
            this.customFieldsTabPage.Controls.Add(this.field3TextBox);
            this.customFieldsTabPage.Controls.Add(this.field8TextBox);
            this.customFieldsTabPage.Controls.Add(this.field2TextBox);
            this.customFieldsTabPage.Controls.Add(this.field1TextBox);
            this.customFieldsTabPage.Location = new System.Drawing.Point(4, 22);
            this.customFieldsTabPage.Name = "customFieldsTabPage";
            this.customFieldsTabPage.Size = new System.Drawing.Size(593, 249);
            this.customFieldsTabPage.TabIndex = 2;
            this.customFieldsTabPage.Text = "Custom";
            this.customFieldsTabPage.UseVisualStyleBackColor = true;
            // 
            // field14Label
            // 
            this.field14Label.AutoSize = true;
            this.field14Label.Location = new System.Drawing.Point(318, 168);
            this.field14Label.Name = "field14Label";
            this.field14Label.Size = new System.Drawing.Size(47, 13);
            this.field14Label.TabIndex = 1;
            this.field14Label.Text = "Field 14:";
            // 
            // field7Label
            // 
            this.field7Label.AutoSize = true;
            this.field7Label.Location = new System.Drawing.Point(10, 168);
            this.field7Label.Name = "field7Label";
            this.field7Label.Size = new System.Drawing.Size(41, 13);
            this.field7Label.TabIndex = 1;
            this.field7Label.Text = "Field 7:";
            // 
            // field6Label
            // 
            this.field6Label.AutoSize = true;
            this.field6Label.Location = new System.Drawing.Point(10, 145);
            this.field6Label.Name = "field6Label";
            this.field6Label.Size = new System.Drawing.Size(41, 13);
            this.field6Label.TabIndex = 1;
            this.field6Label.Text = "Field 6:";
            // 
            // field11Label
            // 
            this.field11Label.AutoSize = true;
            this.field11Label.Location = new System.Drawing.Point(318, 95);
            this.field11Label.Name = "field11Label";
            this.field11Label.Size = new System.Drawing.Size(47, 13);
            this.field11Label.TabIndex = 1;
            this.field11Label.Text = "Field 11:";
            // 
            // field4Label
            // 
            this.field4Label.AutoSize = true;
            this.field4Label.Location = new System.Drawing.Point(10, 95);
            this.field4Label.Name = "field4Label";
            this.field4Label.Size = new System.Drawing.Size(41, 13);
            this.field4Label.TabIndex = 1;
            this.field4Label.Text = "Field 4:";
            // 
            // field9Label
            // 
            this.field9Label.AutoSize = true;
            this.field9Label.Location = new System.Drawing.Point(318, 44);
            this.field9Label.Name = "field9Label";
            this.field9Label.Size = new System.Drawing.Size(41, 13);
            this.field9Label.TabIndex = 1;
            this.field9Label.Text = "Field 9:";
            // 
            // field2Label
            // 
            this.field2Label.AutoSize = true;
            this.field2Label.Location = new System.Drawing.Point(10, 44);
            this.field2Label.Name = "field2Label";
            this.field2Label.Size = new System.Drawing.Size(41, 13);
            this.field2Label.TabIndex = 1;
            this.field2Label.Text = "Field 2:";
            // 
            // field12Label
            // 
            this.field12Label.AutoSize = true;
            this.field12Label.Location = new System.Drawing.Point(318, 119);
            this.field12Label.Name = "field12Label";
            this.field12Label.Size = new System.Drawing.Size(47, 13);
            this.field12Label.TabIndex = 1;
            this.field12Label.Text = "Field 12:";
            // 
            // field5Label
            // 
            this.field5Label.AutoSize = true;
            this.field5Label.Location = new System.Drawing.Point(10, 119);
            this.field5Label.Name = "field5Label";
            this.field5Label.Size = new System.Drawing.Size(41, 13);
            this.field5Label.TabIndex = 1;
            this.field5Label.Text = "Field 5:";
            // 
            // field10Label
            // 
            this.field10Label.AutoSize = true;
            this.field10Label.Location = new System.Drawing.Point(318, 69);
            this.field10Label.Name = "field10Label";
            this.field10Label.Size = new System.Drawing.Size(47, 13);
            this.field10Label.TabIndex = 1;
            this.field10Label.Text = "Field 10:";
            // 
            // field3Label
            // 
            this.field3Label.AutoSize = true;
            this.field3Label.Location = new System.Drawing.Point(10, 69);
            this.field3Label.Name = "field3Label";
            this.field3Label.Size = new System.Drawing.Size(41, 13);
            this.field3Label.TabIndex = 1;
            this.field3Label.Text = "Field 3:";
            // 
            // field8Label
            // 
            this.field8Label.AutoSize = true;
            this.field8Label.Location = new System.Drawing.Point(318, 18);
            this.field8Label.Name = "field8Label";
            this.field8Label.Size = new System.Drawing.Size(41, 13);
            this.field8Label.TabIndex = 1;
            this.field8Label.Text = "Field 8:";
            // 
            // field1Label
            // 
            this.field1Label.AutoSize = true;
            this.field1Label.Location = new System.Drawing.Point(10, 18);
            this.field1Label.Name = "field1Label";
            this.field1Label.Size = new System.Drawing.Size(41, 13);
            this.field1Label.TabIndex = 1;
            this.field1Label.Text = "Field 1:";
            // 
            // field14TextBox
            // 
            this.field14TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field14TextBox.Location = new System.Drawing.Point(369, 165);
            this.field14TextBox.MaxLength = 50;
            this.field14TextBox.Name = "field14TextBox";
            this.field14TextBox.Size = new System.Drawing.Size(205, 20);
            this.field14TextBox.TabIndex = 0;
            // 
            // field7TextBox
            // 
            this.field7TextBox.Location = new System.Drawing.Point(61, 165);
            this.field7TextBox.MaxLength = 50;
            this.field7TextBox.Name = "field7TextBox";
            this.field7TextBox.Size = new System.Drawing.Size(240, 20);
            this.field7TextBox.TabIndex = 0;
            // 
            // field6TextBox
            // 
            this.field6TextBox.Location = new System.Drawing.Point(61, 142);
            this.field6TextBox.MaxLength = 50;
            this.field6TextBox.Name = "field6TextBox";
            this.field6TextBox.Size = new System.Drawing.Size(240, 20);
            this.field6TextBox.TabIndex = 0;
            // 
            // field12TextBox
            // 
            this.field12TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field12TextBox.Location = new System.Drawing.Point(369, 116);
            this.field12TextBox.MaxLength = 50;
            this.field12TextBox.Name = "field12TextBox";
            this.field12TextBox.Size = new System.Drawing.Size(205, 20);
            this.field12TextBox.TabIndex = 0;
            // 
            // field5TextBox
            // 
            this.field5TextBox.Location = new System.Drawing.Point(61, 116);
            this.field5TextBox.MaxLength = 50;
            this.field5TextBox.Name = "field5TextBox";
            this.field5TextBox.Size = new System.Drawing.Size(240, 20);
            this.field5TextBox.TabIndex = 0;
            // 
            // field11TextBox
            // 
            this.field11TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field11TextBox.Location = new System.Drawing.Point(369, 92);
            this.field11TextBox.MaxLength = 50;
            this.field11TextBox.Name = "field11TextBox";
            this.field11TextBox.Size = new System.Drawing.Size(205, 20);
            this.field11TextBox.TabIndex = 0;
            // 
            // field10TextBox
            // 
            this.field10TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field10TextBox.Location = new System.Drawing.Point(369, 66);
            this.field10TextBox.MaxLength = 50;
            this.field10TextBox.Name = "field10TextBox";
            this.field10TextBox.Size = new System.Drawing.Size(205, 20);
            this.field10TextBox.TabIndex = 0;
            // 
            // field4TextBox
            // 
            this.field4TextBox.Location = new System.Drawing.Point(61, 92);
            this.field4TextBox.MaxLength = 50;
            this.field4TextBox.Name = "field4TextBox";
            this.field4TextBox.Size = new System.Drawing.Size(240, 20);
            this.field4TextBox.TabIndex = 0;
            // 
            // field9TextBox
            // 
            this.field9TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field9TextBox.Location = new System.Drawing.Point(369, 41);
            this.field9TextBox.MaxLength = 50;
            this.field9TextBox.Name = "field9TextBox";
            this.field9TextBox.Size = new System.Drawing.Size(205, 20);
            this.field9TextBox.TabIndex = 0;
            // 
            // field3TextBox
            // 
            this.field3TextBox.Location = new System.Drawing.Point(61, 66);
            this.field3TextBox.MaxLength = 50;
            this.field3TextBox.Name = "field3TextBox";
            this.field3TextBox.Size = new System.Drawing.Size(240, 20);
            this.field3TextBox.TabIndex = 0;
            // 
            // field8TextBox
            // 
            this.field8TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field8TextBox.Location = new System.Drawing.Point(369, 15);
            this.field8TextBox.MaxLength = 50;
            this.field8TextBox.Name = "field8TextBox";
            this.field8TextBox.Size = new System.Drawing.Size(205, 20);
            this.field8TextBox.TabIndex = 0;
            // 
            // field2TextBox
            // 
            this.field2TextBox.Location = new System.Drawing.Point(61, 41);
            this.field2TextBox.MaxLength = 100;
            this.field2TextBox.Name = "field2TextBox";
            this.field2TextBox.Size = new System.Drawing.Size(240, 20);
            this.field2TextBox.TabIndex = 0;
            // 
            // field1TextBox
            // 
            this.field1TextBox.Location = new System.Drawing.Point(61, 15);
            this.field1TextBox.MaxLength = 100;
            this.field1TextBox.Name = "field1TextBox";
            this.field1TextBox.Size = new System.Drawing.Size(240, 20);
            this.field1TextBox.TabIndex = 0;
            // 
            // memoTabPage
            // 
            this.memoTabPage.Controls.Add(this.field13Label);
            this.memoTabPage.Controls.Add(this.field13TextBox);
            this.memoTabPage.Location = new System.Drawing.Point(4, 22);
            this.memoTabPage.Name = "memoTabPage";
            this.memoTabPage.Size = new System.Drawing.Size(593, 249);
            this.memoTabPage.TabIndex = 3;
            this.memoTabPage.Text = "Memo";
            this.memoTabPage.UseVisualStyleBackColor = true;
            // 
            // field13Label
            // 
            this.field13Label.AutoSize = true;
            this.field13Label.Location = new System.Drawing.Point(3, 11);
            this.field13Label.Name = "field13Label";
            this.field13Label.Size = new System.Drawing.Size(47, 13);
            this.field13Label.TabIndex = 3;
            this.field13Label.Text = "Field 13:";
            // 
            // field13TextBox
            // 
            this.field13TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.field13TextBox.Location = new System.Drawing.Point(6, 27);
            this.field13TextBox.MaxLength = 0;
            this.field13TextBox.Multiline = true;
            this.field13TextBox.Name = "field13TextBox";
            this.field13TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.field13TextBox.Size = new System.Drawing.Size(598, 206);
            this.field13TextBox.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(619, 41);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtPinLength
            // 
            this.txtPinLength.Location = new System.Drawing.Point(144, 223);
            this.txtPinLength.Name = "txtPinLength";
            this.txtPinLength.Size = new System.Drawing.Size(29, 20);
            this.txtPinLength.TabIndex = 22;
            this.txtPinLength.Text = "4";
            this.txtPinLength.TextChanged += new System.EventHandler(this.txtPinLength_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "PIN length:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // UserRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 406);
            this.Controls.Add(this.newUserTabControl);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.newUserStatusStrip);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.MinimumSize = new System.Drawing.Size(628, 433);
            this.Name = "UserRecord";
            this.Text = "OemClientNewUser";
            this.newUserStatusStrip.ResumeLayout(false);
            this.newUserStatusStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.newUserTabControl.ResumeLayout(false);
            this.userTabPage.ResumeLayout(false);
            this.userTabPage.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.accessLevelTabPage.ResumeLayout(false);
            this.accessLevelTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readerAreasGridView)).EndInit();
            this.customFieldsTabPage.ResumeLayout(false);
            this.customFieldsTabPage.PerformLayout();
            this.memoTabPage.ResumeLayout(false);
            this.memoTabPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label surnameLabel;
		private System.Windows.Forms.Label middleNameLabel;
		private System.Windows.Forms.Label firstNameLabel;
		private System.Windows.Forms.TextBox middleNameTextBox;
		private System.Windows.Forms.TextBox surnameTextBox;
		private System.Windows.Forms.TextBox firstNameTextBox;
		private System.Windows.Forms.CheckBox alarmUserCheckBox;
		private System.Windows.Forms.CheckBox antiPassbackCheckBox;
		private System.Windows.Forms.ComboBox accessLevelComboBox;
		private System.Windows.Forms.ComboBox departmentComboBox;
		private System.Windows.Forms.Label accessLevelLabel;
		private System.Windows.Forms.Label departmentLabel;
		private System.Windows.Forms.Label extensionLabel;
		private System.Windows.Forms.Label telephoneLabel;
		private System.Windows.Forms.TextBox telephoneExtTextBox;
        private System.Windows.Forms.TextBox telephoneNoTextBox;
		private System.Windows.Forms.StatusStrip newUserStatusStrip;
		private System.Windows.Forms.ToolStripStatusLabel newUserStatusLabel;
		private System.Windows.Forms.Button generatePinButton;
        private System.Windows.Forms.TextBox pinTextBox;
		private System.Windows.Forms.TextBox pictureFileTextBox;
		private System.Windows.Forms.Label pictureFileLabel;
		private System.Windows.Forms.Label cardNumberLabel;
        private System.Windows.Forms.TextBox cardNumberTextBox;
		private System.Windows.Forms.Button addCardButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button deleteCardButton;
		private System.Windows.Forms.ComboBox cardsComboBox;
		private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.CheckBox activeCheckBox;
		private System.Windows.Forms.TabControl newUserTabControl;
		private System.Windows.Forms.TabPage userTabPage;
		private System.Windows.Forms.TabPage accessLevelTabPage;
		private System.Windows.Forms.RadioButton individualRadioButton;
		private System.Windows.Forms.RadioButton accessLevelRadioButton;
		private System.Windows.Forms.DataGridView readerAreasGridView;
		private System.Windows.Forms.TabPage customFieldsTabPage;
		private System.Windows.Forms.Label field1Label;
		private System.Windows.Forms.TextBox field1TextBox;
		private System.Windows.Forms.Label field14Label;
		private System.Windows.Forms.Label field7Label;
		private System.Windows.Forms.Label field6Label;
		private System.Windows.Forms.Label field11Label;
		private System.Windows.Forms.Label field4Label;
		private System.Windows.Forms.Label field9Label;
		private System.Windows.Forms.Label field2Label;
		private System.Windows.Forms.Label field12Label;
		private System.Windows.Forms.Label field5Label;
		private System.Windows.Forms.Label field10Label;
		private System.Windows.Forms.Label field3Label;
		private System.Windows.Forms.Label field8Label;
		private System.Windows.Forms.TextBox field14TextBox;
		private System.Windows.Forms.TextBox field7TextBox;
		private System.Windows.Forms.TextBox field6TextBox;
		private System.Windows.Forms.TextBox field12TextBox;
		private System.Windows.Forms.TextBox field5TextBox;
		private System.Windows.Forms.TextBox field11TextBox;
		private System.Windows.Forms.TextBox field10TextBox;
		private System.Windows.Forms.TextBox field4TextBox;
		private System.Windows.Forms.TextBox field9TextBox;
		private System.Windows.Forms.TextBox field3TextBox;
		private System.Windows.Forms.TextBox field8TextBox;
		private System.Windows.Forms.TextBox field2TextBox;
		private System.Windows.Forms.TabPage memoTabPage;
		private System.Windows.Forms.Label field13Label;
		private System.Windows.Forms.TextBox field13TextBox;
		private System.Windows.Forms.DateTimePicker expiryDate;
		private System.Windows.Forms.TextBox faxTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox expiresCheckBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker activationDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPinLength;
        private System.Windows.Forms.Label label4;
	}
}