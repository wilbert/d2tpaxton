using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
    public partial class OpenDoor : UserControl
    {
        private OemClient _net2Client;
        private IDoors _doorsSet;
        
        public OpenDoor()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The OpenDoor user control uses
        /// </summary>
        /// <param name="net2Client"></param>
        public void Initialise(OemClient net2Client)
        {
            _net2Client = net2Client;
            LoadDoorsList();            
        }

        public void Close()
        {
            if (_doorsSet != null)
            {
                _doorsSet.DoorsDataSource.Dispose();
                _doorsSet = null;
            }

            if (_net2Client != null)
            {
                _net2Client.Dispose();
                _net2Client = null;
            }

            Dispose();
        }

        /// <summary>
        /// Open a single door by passing in the name of the access control unit.
        /// </summary>
        private void btnOpenDoorByName_Click(object sender, EventArgs e)
        {
            string doorName = txtDoorName.Text;
                        
            if (_net2Client.OpenDoor(doorName))
            {
                Console.WriteLine(string.Format("Door named {0} was opened successfully.", doorName));
            }
            else
            {
                Console.WriteLine(string.Format("Door named {0} failed to open!", doorName));
            }
        }

        /// <summary>
        /// Open a door (or doors) by passing an array of ACU serial numbers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenDoorByAddress_Click(object sender, EventArgs e)
        {
            List<int> addressList = new List<int>();
            
            foreach (ListViewItem itm in lstDoors.Items)
            {
                if (itm.Selected)
                {
                    int address = int.Parse(itm.SubItems[1].Text);
                    addressList.Add(address);
                }
            }
        
            bool blnResult = _net2Client.OpenDoor(addressList.ToArray());
            if (blnResult)
            {
                Console.WriteLine("The selected doors were opened successfully.");
            }
            else
            {
                Console.WriteLine("There was a problem opening the selected doors!");
            }

            if (addressList.Count > 0)
            {
                addressList.Clear();
            }

            addressList = null;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadDoorsList();
        }

        private void LoadDoorsList()
        {
            lstDoors.Items.Clear();
            _doorsSet = _net2Client.ViewDoors();
            if (_doorsSet != null)
            {
                foreach (IDoorView door in _doorsSet.DoorsList())
                {
                    string doorName = door.Name;   // holds the name of the control unit
                    int serialNumber = door.Address;    // holds the Address (serial number) of the control unit

                    ListViewItem itm = new ListViewItem(doorName);
                    itm.SubItems.Add(serialNumber.ToString("D9"));

                    lstDoors.Items.Add(itm);
                }
            }
        }

        /// <summary>
        /// When items are selected from the list of available doors, enter their details
        /// into the text boxes, to demonstrate the use of 'Open Door By Name' and 'Open Door By Address' 
        /// Note that it is possible to open multiple doors by address
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstDoors_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView doorList = (ListView) sender;
            
            txtDoorAddress.Text = "";
            txtDoorName.Text = "";

            foreach (ListViewItem itm in doorList.Items)
            {
                if (itm.Selected)
                {
                    // If multiple doors are selected, then allow addresses to be comma separated
                    if (txtDoorAddress.Text != "")
                    {
                        txtDoorAddress.Text += ", ";
                    }
                    txtDoorAddress.Text += itm.SubItems[1].Text;

                    // Display the last door which was clicked
                    txtDoorName.Text = itm.Text;
                }
            }
        }

        private void btnHoldDoorOpen_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in lstDoors.Items)
            {
                if (itm.Selected)
                {
                    int address = int.Parse(itm.SubItems[1].Text);
                    bool blnResult = _net2Client.HoldDoorOpen(address);
                    if (blnResult)
                    {
                        Console.WriteLine(string.Format("Door {0} was opened successfully.", address));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Door {0} could not be opened.", address));
                    }
                }
            }
        }

        private void btnOpenDoorFor_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in lstDoors.Items)
            {
                if (itm.Selected)
                {
                    int address = int.Parse(itm.SubItems[1].Text);
                    bool blnResult = _net2Client.OpenDoor(address, int.Parse(udDuration.Value.ToString()) * 1000);
                    if (blnResult)
                    {
                        Console.WriteLine(string.Format("Door {0} was opened successfully.", address));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Door {0} could not be opened.", address));
                    }
                }
            }
        }

        private void btnCloseDoor_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in lstDoors.Items)
            {
                if (itm.Selected)
                {
                    int address = int.Parse(itm.SubItems[1].Text);
                    bool blnResult = _net2Client.CloseDoor(address);
                    if (blnResult)
                    {
                        Console.WriteLine(string.Format("Door {0} was closed successfully.", address));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Door {0} could not be closed.", address));
                    }
                }
            }
        }
    }
}
