namespace Net2_OEM_SDK
{
    partial class OpenDoor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDoorAddress = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDoorName = new System.Windows.Forms.TextBox();
            this.btnOpenDoorByName = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lstDoors = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnHoldDoorOpen = new System.Windows.Forms.Button();
            this.btnOpenDoorFor = new System.Windows.Forms.Button();
            this.btnCloseDoor = new System.Windows.Forms.Button();
            this.udDuration = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udDuration)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.txtDoorAddress);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Location = new System.Drawing.Point(5, 258);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 182);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Open door by address";
            // 
            // txtDoorAddress
            // 
            this.txtDoorAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDoorAddress.Location = new System.Drawing.Point(6, 19);
            this.txtDoorAddress.Name = "txtDoorAddress";
            this.txtDoorAddress.Size = new System.Drawing.Size(412, 20);
            this.txtDoorAddress.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(432, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Open door";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnOpenDoorByAddress_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtDoorName);
            this.groupBox1.Controls.Add(this.btnOpenDoorByName);
            this.groupBox1.Location = new System.Drawing.Point(5, 201);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 51);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Open door by name";
            // 
            // txtDoorName
            // 
            this.txtDoorName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDoorName.Location = new System.Drawing.Point(6, 19);
            this.txtDoorName.Name = "txtDoorName";
            this.txtDoorName.Size = new System.Drawing.Size(412, 20);
            this.txtDoorName.TabIndex = 1;
            // 
            // btnOpenDoorByName
            // 
            this.btnOpenDoorByName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenDoorByName.Location = new System.Drawing.Point(432, 17);
            this.btnOpenDoorByName.Name = "btnOpenDoorByName";
            this.btnOpenDoorByName.Size = new System.Drawing.Size(75, 23);
            this.btnOpenDoorByName.TabIndex = 1;
            this.btnOpenDoorByName.Text = "Open door";
            this.btnOpenDoorByName.UseVisualStyleBackColor = true;
            this.btnOpenDoorByName.Click += new System.EventHandler(this.btnOpenDoorByName_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(443, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lstDoors
            // 
            this.lstDoors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDoors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lstDoors.FullRowSelect = true;
            this.lstDoors.GridLines = true;
            this.lstDoors.Location = new System.Drawing.Point(3, 3);
            this.lstDoors.Name = "lstDoors";
            this.lstDoors.Size = new System.Drawing.Size(434, 192);
            this.lstDoors.TabIndex = 5;
            this.lstDoors.UseCompatibleStateImageBehavior = false;
            this.lstDoors.View = System.Windows.Forms.View.Details;
            this.lstDoors.SelectedIndexChanged += new System.EventHandler(this.lstDoors_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Door name";
            this.columnHeader1.Width = 99;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Serial number";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 99;
            // 
            // btnHoldDoorOpen
            // 
            this.btnHoldDoorOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHoldDoorOpen.Location = new System.Drawing.Point(263, 33);
            this.btnHoldDoorOpen.Name = "btnHoldDoorOpen";
            this.btnHoldDoorOpen.Size = new System.Drawing.Size(109, 23);
            this.btnHoldDoorOpen.TabIndex = 1;
            this.btnHoldDoorOpen.Text = "Hold door open";
            this.btnHoldDoorOpen.UseVisualStyleBackColor = true;
            this.btnHoldDoorOpen.Click += new System.EventHandler(this.btnHoldDoorOpen_Click);
            // 
            // btnOpenDoorFor
            // 
            this.btnOpenDoorFor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenDoorFor.Location = new System.Drawing.Point(263, 62);
            this.btnOpenDoorFor.Name = "btnOpenDoorFor";
            this.btnOpenDoorFor.Size = new System.Drawing.Size(109, 23);
            this.btnOpenDoorFor.TabIndex = 1;
            this.btnOpenDoorFor.Text = "Open door for...";
            this.btnOpenDoorFor.UseVisualStyleBackColor = true;
            this.btnOpenDoorFor.Click += new System.EventHandler(this.btnOpenDoorFor_Click);
            // 
            // btnCloseDoor
            // 
            this.btnCloseDoor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseDoor.Location = new System.Drawing.Point(263, 91);
            this.btnCloseDoor.Name = "btnCloseDoor";
            this.btnCloseDoor.Size = new System.Drawing.Size(109, 23);
            this.btnCloseDoor.TabIndex = 1;
            this.btnCloseDoor.Text = "Close door";
            this.btnCloseDoor.UseVisualStyleBackColor = true;
            this.btnCloseDoor.Click += new System.EventHandler(this.btnCloseDoor_Click);
            // 
            // udDuration
            // 
            this.udDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.udDuration.Location = new System.Drawing.Point(378, 65);
            this.udDuration.Name = "udDuration";
            this.udDuration.Size = new System.Drawing.Size(57, 20);
            this.udDuration.TabIndex = 9;
            this.udDuration.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(441, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "seconds";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btnHoldDoorOpen);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnOpenDoorFor);
            this.groupBox3.Controls.Add(this.udDuration);
            this.groupBox3.Controls.Add(this.btnCloseDoor);
            this.groupBox3.Location = new System.Drawing.Point(6, 46);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(501, 129);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Enhanced features";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(15, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 81);
            this.label2.TabIndex = 11;
            this.label2.Text = "The \'Open door\' feature will simply cause the door to open for the door open time" +
                ".  However, it may be required to hold the doors open, or to open them for a dur" +
                "ation other than the door open time.";
            // 
            // OpenDoor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lstDoors);
            this.MinimumSize = new System.Drawing.Size(521, 445);
            this.Name = "OpenDoor";
            this.Size = new System.Drawing.Size(521, 445);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udDuration)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDoorAddress;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDoorName;
        private System.Windows.Forms.Button btnOpenDoorByName;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListView lstDoors;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnCloseDoor;
        private System.Windows.Forms.Button btnOpenDoorFor;
        private System.Windows.Forms.Button btnHoldDoorOpen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udDuration;
        private System.Windows.Forms.Label label2;
    }
}
