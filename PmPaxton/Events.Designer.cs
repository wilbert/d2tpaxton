namespace Net2_OEM_SDK
{
    partial class Events
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRefresh = new System.Windows.Forms.Button();
            this.EventsView = new System.Windows.Forms.ListView();
            this.eventDate = new System.Windows.Forms.ColumnHeader();
            this.eventTime = new System.Windows.Forms.ColumnHeader();
            this.eventFirstName = new System.Windows.Forms.ColumnHeader();
            this.eventMiddleName = new System.Windows.Forms.ColumnHeader();
            this.eventSurname = new System.Windows.Forms.ColumnHeader();
            this.eventDeviceName = new System.Windows.Forms.ColumnHeader();
            this.eventDescription = new System.Windows.Forms.ColumnHeader();
            this.eventSubDescription = new System.Windows.Forms.ColumnHeader();
            this.todaysEventsButton = new System.Windows.Forms.Button();
            this.eventsFilterButton = new System.Windows.Forms.Button();
            this.eventsWhereClauseText = new System.Windows.Forms.TextBox();
            this.whereClauseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(484, 311);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(91, 25);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Last Ten";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // EventsView
            // 
            this.EventsView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.eventDate,
            this.eventTime,
            this.eventFirstName,
            this.eventMiddleName,
            this.eventSurname,
            this.eventDeviceName,
            this.eventDescription,
            this.eventSubDescription});
            this.EventsView.FullRowSelect = true;
            this.EventsView.GridLines = true;
            this.EventsView.Location = new System.Drawing.Point(3, 3);
            this.EventsView.Name = "EventsView";
            this.EventsView.Size = new System.Drawing.Size(572, 302);
            this.EventsView.TabIndex = 2;
            this.EventsView.UseCompatibleStateImageBehavior = false;
            this.EventsView.View = System.Windows.Forms.View.Details;
            // 
            // eventDate
            // 
            this.eventDate.Text = "Date";
            this.eventDate.Width = 51;
            // 
            // eventTime
            // 
            this.eventTime.Text = "Time";
            this.eventTime.Width = 45;
            // 
            // eventFirstName
            // 
            this.eventFirstName.Text = "First name";
            this.eventFirstName.Width = 64;
            // 
            // eventMiddleName
            // 
            this.eventMiddleName.Text = "Middle name";
            this.eventMiddleName.Width = 81;
            // 
            // eventSurname
            // 
            this.eventSurname.Text = "Surname";
            this.eventSurname.Width = 68;
            // 
            // eventDeviceName
            // 
            this.eventDeviceName.Text = "Device name";
            this.eventDeviceName.Width = 85;
            // 
            // eventDescription
            // 
            this.eventDescription.Text = "Description";
            this.eventDescription.Width = 70;
            // 
            // eventSubDescription
            // 
            this.eventSubDescription.Text = "Sub description";
            this.eventSubDescription.Width = 92;
            // 
            // todaysEventsButton
            // 
            this.todaysEventsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.todaysEventsButton.Location = new System.Drawing.Point(384, 311);
            this.todaysEventsButton.Name = "todaysEventsButton";
            this.todaysEventsButton.Size = new System.Drawing.Size(94, 25);
            this.todaysEventsButton.TabIndex = 3;
            this.todaysEventsButton.Text = "Today\'s Events";
            this.todaysEventsButton.UseVisualStyleBackColor = true;
            this.todaysEventsButton.Click += new System.EventHandler(this.todaysEventsButton_Click);
            // 
            // eventsFilterButton
            // 
            this.eventsFilterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eventsFilterButton.Enabled = false;
            this.eventsFilterButton.Location = new System.Drawing.Point(3, 311);
            this.eventsFilterButton.Name = "eventsFilterButton";
            this.eventsFilterButton.Size = new System.Drawing.Size(75, 23);
            this.eventsFilterButton.TabIndex = 4;
            this.eventsFilterButton.Text = "Filter";
            this.eventsFilterButton.UseVisualStyleBackColor = true;
            this.eventsFilterButton.Click += new System.EventHandler(this.eventsFilterButton_Click);
            // 
            // eventsWhereClauseText
            // 
            this.eventsWhereClauseText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.eventsWhereClauseText.Location = new System.Drawing.Point(159, 311);
            this.eventsWhereClauseText.Name = "eventsWhereClauseText";
            this.eventsWhereClauseText.Size = new System.Drawing.Size(219, 20);
            this.eventsWhereClauseText.TabIndex = 5;
            this.eventsWhereClauseText.TextChanged += new System.EventHandler(this.eventsWhereClauseText_TextChanged);
            // 
            // whereClauseLabel
            // 
            this.whereClauseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.whereClauseLabel.AutoSize = true;
            this.whereClauseLabel.Location = new System.Drawing.Point(82, 316);
            this.whereClauseLabel.Name = "whereClauseLabel";
            this.whereClauseLabel.Size = new System.Drawing.Size(77, 13);
            this.whereClauseLabel.TabIndex = 6;
            this.whereClauseLabel.Text = "Where Clause:";
            // 
            // Events
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.whereClauseLabel);
            this.Controls.Add(this.eventsWhereClauseText);
            this.Controls.Add(this.eventsFilterButton);
            this.Controls.Add(this.todaysEventsButton);
            this.Controls.Add(this.EventsView);
            this.Controls.Add(this.btnRefresh);
            this.Name = "Events";
            this.Size = new System.Drawing.Size(578, 339);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListView EventsView;
        private System.Windows.Forms.ColumnHeader eventDate;
        private System.Windows.Forms.ColumnHeader eventTime;
        private System.Windows.Forms.ColumnHeader eventFirstName;
        private System.Windows.Forms.ColumnHeader eventMiddleName;
        private System.Windows.Forms.ColumnHeader eventSurname;
        private System.Windows.Forms.ColumnHeader eventDeviceName;
        private System.Windows.Forms.ColumnHeader eventDescription;
        private System.Windows.Forms.ColumnHeader eventSubDescription;
		private System.Windows.Forms.Button todaysEventsButton;
		private System.Windows.Forms.Button eventsFilterButton;
		private System.Windows.Forms.TextBox eventsWhereClauseText;
		private System.Windows.Forms.Label whereClauseLabel;
    }
}
