using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
    public partial class UserRecords : UserControl
    {
        private OemClient _net2Client;
        private IUsers _usersSet;
        private UserRecord _userRecord;
        Dictionary<int, IUserView> _usersList;
        private BindingSource _usersSource = new BindingSource ();
        
        public UserRecords()
        {
            InitializeComponent();
        }

        public void Initialise(OemClient net2Client)
        {
            _net2Client = net2Client;
            LoadUsers(null);
        }

        private void LoadUsers(string filter)
        {
            if (_usersSet != null)
            {
                _usersSet.UsersDataSource.Dispose();
                _usersSet = null;
            }

            if (_usersSet == null)
            {
				if (filter == null)
				{
					_usersSet = _net2Client.ViewUserRecords();
				}
				else
				{
					_usersSet = _net2Client.ViewUserRecords (filter);
				}
            }

            if (_usersSet != null)
            {
                _usersList = _usersSet.UsersList();

				lvwUserRecords.Items.Clear ();
                foreach (KeyValuePair<int, IUserView> idx in _usersList)
                {
                    IUserView user = idx.Value;
                    ListViewItem itm = lvwUserRecords.Items.Add(user.UserId.ToString());
                    itm.SubItems.Add(user.FirstName);
                    itm.SubItems.Add(user.Surname);
                    itm.SubItems.Add(user.Department);
                    itm.SubItems.Add(user.AccessLevel);
                }
            }
            else
            {
                Console.WriteLine(_net2Client.LastErrorMessage);
            }
        }

        private void lvwUserRecords_DoubleClick(object sender, EventArgs e)
        {
            ListView lvw = (ListView)sender;
            int userID = int.Parse(lvw.SelectedItems[0].Text);
            Console.WriteLine(string.Format("User ID {0} selected", userID));
            SelectUserRecord(userID);
        }

        private void btnEditUser_Click(object sender, EventArgs e)
        {
			lvwUserRecords_DoubleClick (this.lvwUserRecords, e);
        }

        private void SelectUserRecord(int userID)
        {
            if (_userRecord != null)
            {
                _userRecord.Close();
                _userRecord = null;
            }

            IUserView user = _usersList[userID];

            _userRecord = new UserRecord(_net2Client, user);
            _userRecord.Activate();
            _userRecord.Visible = true;
        }

        private void btnNewUser_Click(object sender, EventArgs e)
        {
			if (_userRecord != null)
			{
				_userRecord.Close ();
				_userRecord = null;
			}

			_userRecord = new UserRecord (_net2Client);
			_userRecord.Activate ();
			_userRecord.Visible = true;
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
			ListView lvw = this.lvwUserRecords;
            int userID = int.Parse(lvw.SelectedItems[0].Text);
            Console.WriteLine(string.Format("User ID {0} selected", userID));
			if (_userRecord != null)
			{
				_userRecord.Close ();
				_userRecord = null;
			}

			IUserView user = _usersList[userID];

			// To delete or bar a user, supply the required UserID and set the Active indicator to false.
			// Set all value types to their existing values and string types to null. This will delete all cards issued to this user.
			bool blnResult = _net2Client.UpdateUserRecord (userID, 0, user.DepartmentId, false, false, null, null, null, null, null, null, null,
															user.ActivationDate, false, null, user.ExpiryDate, null);
			if (blnResult)
			{
				Console.WriteLine ("User was barred.");
			}
			else
			{
				Console.WriteLine ("User bar failed: {0}", _net2Client.LastErrorMessage);
			}
        }

		private void refreshUsersButton_Click (object sender, EventArgs e)
		{
			LoadUsers (null);
			userIdWhereClauseText.Text = "";
			surnameWhereClauseText.Text = "";
		}

		private void lvwUserRecords_SelectedIndexChanged (object sender, EventArgs e)
		{
            ListView lvw = (ListView)sender;
            if (lvw.SelectedItems.Count > 0)
            {
                int userID = int.Parse(lvw.SelectedItems[0].Text);
                Console.WriteLine(string.Format("User ID {0} selected", userID));
                IUserView user = _usersList[userID];

                userIdWhereClauseText.Text = "UserID = " + userID.ToString();
                surnameWhereClauseText.Text = string.Format("Surname = '{0}'", user.Surname);
            }
		}

		private void userIdFilterButton_Click (object sender, EventArgs e)
		{
			string userIdFilter = userIdWhereClauseText.Text;
			if (!string.IsNullOrEmpty (userIdFilter))
			{
				LoadUsers (userIdFilter);
			}
		}

		private void surnameFilterButton_Click (object sender, EventArgs e)
		{
			string surnameFilter = surnameWhereClauseText.Text;
			if (!string.IsNullOrEmpty (surnameFilter))
			{
				LoadUsers (surnameFilter);
			}
		}
    }
}
