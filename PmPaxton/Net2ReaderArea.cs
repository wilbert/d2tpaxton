using System;
using System.Collections.Generic;
using System.Text;
using Paxton.Net2.OemClientLibrary;

namespace Net2_OEM_SDK
{
	public class Net2ReaderArea
	{
		private int _areaId;
		private string _readerAreaName;
		private int _timezoneId;

		public Net2ReaderArea (IReaderAreaView newReaderArea)
		{
			_areaId = newReaderArea.AreaId;
			_readerAreaName = newReaderArea.ReaderAreaName;
			_timezoneId = newReaderArea.TimezoneId;
		}

		public int AreaId
		{
			get
			{
				return _areaId;
			}
			set
			{
				_areaId = value;
			}
		}

		public string ReaderAreaName
		{
			get
			{
				return _readerAreaName;
			}
			set
			{
				_readerAreaName = value;
			}
		}

		public int TimezoneId
		{
			get
			{
				return _timezoneId;
			}
			set
			{
				_timezoneId = value;
			}
		}
	}
}
