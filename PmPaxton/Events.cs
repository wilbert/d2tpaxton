using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Paxton.Net2.OemClientLibrary;
using System.Threading;
using System.Globalization;

namespace Net2_OEM_SDK
{
    public partial class Events : UserControl
    {
        private OemClient _net2Client;
        

        public Events()
		{
            InitializeComponent ();
        }

        public void Initialise(OemClient thisClient)
        {
			_net2Client = thisClient;
		}
               

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            int maxCount = 1000;
            string sortDirection = "desc";
            IEvents eventsSet;
                                    
			eventsSet = _net2Client.ViewEvents (maxCount, sortDirection);
			PopulateEventsList (eventsSet);
        }

		private void todaysEventsButton_Click (object sender, EventArgs e)
		{


            int maxCount = int.MaxValue;
            string orderBy = "EventTime desc, EventID desc";
			string todayAsWhereClause = string.Format ("EventDateTime between '{0:yyyy-MM-dd} 00:00:00' and '{0:yyyy-MM-dd} 23:59:59'", DateTime.Today);

			IEvents eventsSet = _net2Client.ViewEvents (maxCount, todayAsWhereClause, orderBy);
			PopulateEventsList (eventsSet);
		}

		private void PopulateEventsList (IEvents eventsSet)
		{
			DataSet ds = (DataSet)eventsSet.EventsDataSource;

			DataTable eventsTable = ds.Tables["Event"];
			DataTableReader dr = ds.CreateDataReader (eventsTable);
			ListViewItem nextEvent;

			this.EventsView.Items.Clear ();
			while (dr.Read ())
			{
				nextEvent = new ListViewItem (string.Format ("{0:dd/mm/yyyy}", dr["EventDate"]));

				nextEvent.SubItems.Add (string.Format ("{0:hh:mm:ss}", dr["EventTime"]));
				nextEvent.SubItems.Add ((dr["FirstName"] == DBNull.Value) ? "" : dr["FirstName"].ToString ());
				nextEvent.SubItems.Add ((dr["MiddleName"] == DBNull.Value) ? "" : dr["MiddleName"].ToString ());
				nextEvent.SubItems.Add ((dr["Surname"] == DBNull.Value) ? "" : dr["Surname"].ToString ());
				nextEvent.SubItems.Add ((dr["DeviceName"] == DBNull.Value) ? "" : dr["DeviceName"].ToString ());
				nextEvent.SubItems.Add (dr["EventDescription"].ToString ());
				nextEvent.SubItems.Add ((dr["EventSubDescription"] == DBNull.Value) ? "" : dr["EventSubDescription"].ToString ());

				this.EventsView.Items.Add (nextEvent);
			}
		}

		private void eventsWhereClauseText_TextChanged (object sender, EventArgs e)
		{
			TextBox thisText = (TextBox)sender;
			if (string.IsNullOrEmpty (thisText.Text))
			{
				eventsFilterButton.Enabled = false;
			}
			else
			{
				eventsFilterButton.Enabled = true;
			}
		}

		private void eventsFilterButton_Click (object sender, EventArgs e)
		{
            //string query = "SELECT * from EventTypes";
            //DataSet ds = _net2Client.QueryDb(query); 
            IEvents eventsSet;
            int maxCount = int.MaxValue;
			string orderBy = "EventDate asc, EventID asc";
			string whereClause = this.eventsWhereClauseText.Text;

			eventsSet = _net2Client.ViewEvents (maxCount, whereClause, orderBy);
			if (eventsSet != null)
			{
				PopulateEventsList (eventsSet);
			}
			else
			{
				Console.WriteLine ("Events filter error: {0}", _net2Client.LastErrorMessage);
			}
		}
    }
}
