using Paxton.Net2.OemClientLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Net2_OEM_SDK
{
	public partial class UserRecord : Form
	{
		const int CUSTOM_FIELD_COUNT = 14;
		private IDepartments _departmentsSet;
		private IAccessLevels _accessLevelsSet;
		private IIndividualReaderAreas _readerAreasSet;
		private OemClient _thisClient;
		private IUserView _thisUser;
		private Dictionary<int, string> _customFieldChanges;
		private Dictionary<int, string> _itemsList;
		private Dictionary<int, IReaderAreaView> _readerAreasList;
		private BindingSource _readerAreaSource;
        private Dictionary<int, Net2ReaderArea> _rdrAreaList;
        
		public UserRecord (OemClient currentClient)
		{
			_thisClient = currentClient;

			Initialise ();
			PopulateAccessLevels ();
			addCardButton.Enabled = false;
			btnApply.Enabled = true;
			activationDate.Value = DateTime.Today;
		}

		public UserRecord (OemClient currentClient, IUserView updatableUser)
		{
			_thisClient = currentClient;
			_thisUser = updatableUser;
			Initialise ();
			PopulateUserDetails ();
			PopulateCustomFields ();

			if (_thisUser.AccessLevelId > 100000000)
			{
				individualRadioButton.Checked = true;
				accessLevelRadioButton.Checked = false;
			}
			else
			{
				individualRadioButton.Checked = false;
				accessLevelRadioButton.Checked = true;
			}
			if (accessLevelRadioButton.Checked)
			{
				PopulateAccessLevels ();
			}
			else
			{
				PopulateIndividualReaderAreas ();
			}

			addCardButton.Enabled = true;
			btnApply.Enabled = true;
		}

		private void Initialise()
		{
			InitializeComponent ();

			newUserStatusLabel.Text = "";
			_departmentsSet = _thisClient.ViewDepartments ();
			if (_departmentsSet != null)
			{
				_itemsList = _departmentsSet.DepartmentsDictionary ();
				int selectedDept = -1;
				if (_thisUser != null)
				{
					selectedDept = _thisUser.DepartmentId;
				}
				PopulateComboList (departmentComboBox, selectedDept);
				_itemsList.Clear ();
			}
			else
			{
				newUserStatusLabel.Text = _thisClient.LastErrorMessage;
			}

            //cboCardType.Items.Add("Key");
            //cboCardType.Items.Add("Card");
            //cboCardType.Items.Add("ISO Card");
            //cboCardType.Items.Add("Keyfob");
            //cboCardType.Items.Add("Net2Air");
            //cboCardType.Items.Add("WatchProx");
            
            //_cardTypesSet = _thisClient.CardTypesList ();
            //if (_cardTypesSet != null)
            //{
            //    _itemsList = _cardTypesSet.CardTypesDictionary ();
            //    //PopulateComboList(cardTypesComboBox, -1);
            //    _itemsList.Clear();
            //}
            //else
            //{
            //    newUserStatusLabel.Text = _thisClient.LastErrorMessage;
            //}
			
			expiryDate.Value = DateTime.Today;
			Visible = true;
		}

		private void PopulateUserDetails ()
		{
			try
			{
				firstNameTextBox.Text = _thisUser.FirstName;
				middleNameTextBox.Text = _thisUser.MiddleName;
				surnameTextBox.Text = _thisUser.Surname;
				departmentComboBox.SelectedValue = _thisUser.DepartmentId;
				telephoneNoTextBox.Text = _thisUser.Telephone;
				telephoneExtTextBox.Text = _thisUser.Extension;
				faxTextBox.Text = _thisUser.Fax;
				pictureFileTextBox.Text = _thisUser.Picture;
				antiPassbackCheckBox.Checked = _thisUser.AntiPassbackUser;
				alarmUserCheckBox.Checked = _thisUser.AlarmUser;
				activeCheckBox.Checked = _thisUser.Active;
				pinTextBox.Text = _thisUser.PIN;
				activationDate.Value = _thisUser.ActivationDate;
				if (_thisUser.ExpiryDate == DateTime.MinValue)
				{
					expiresCheckBox.Checked = false;
					expiryDate.Enabled = false;
				}
				else
				{
					expiresCheckBox.Checked = true;
					expiryDate.Enabled = true;
					expiryDate.Value = _thisUser.ExpiryDate;
				}

				PopulateCardsList ();
			}
			catch (Exception excp)
			{
				Console.WriteLine(excp.Message);
			}
		}

		private void PopulateComboList (ComboBox thisCombo, int selectedValue)
		{
            int thisIndex = 0;
            int selectedIndex = 0;

			thisCombo.Items.Clear ();
			thisCombo.DisplayMember = "DisplayName";
			thisCombo.ValueMember = "ListId";
			
			
			foreach (int itemId in _itemsList.Keys)
			{
                if (itemId < 100000000)
                {
                    ListItem thisItem = new ListItem(itemId, _itemsList[itemId]);
                    thisCombo.Items.Add(thisItem);
                    if (thisItem.ListId == selectedValue)
                    {
                        selectedIndex = thisIndex;
                    }
                    thisIndex++;
                }
			}

			thisCombo.SelectedIndex = selectedIndex;
		}

		private int DeriveSelectedComboValue (ComboBox thisCombo)
		{
            try
            {
                int selectedIdx = thisCombo.SelectedIndex;
                ListItem selectedItem = (ListItem)thisCombo.Items[selectedIdx];
                return selectedItem.ListId;
            }
            catch (Exception)
            {
                return -1;
            }
		}

		private void PopulateCardsList ()
		{
			List<int> cardsForUser = _thisClient.ViewCards (_thisUser.UserId);
			cardsComboBox.Items.Clear ();
			cardsComboBox.Text = "";
			if (cardsForUser != null)
			{
				foreach (int cardNo in cardsForUser)
				{
					cardsComboBox.Items.Add (cardNo.ToString ());
				}

				cardsForUser.Clear ();
				cardsForUser = null;

				if (cardsComboBox.Items.Count > 0)
				{
					cardsComboBox.SelectedIndex = 0;
				}
			}
			else
			{
				newUserStatusLabel.Text = _thisClient.LastErrorMessage;
			}
		}

		private void PopulateIndividualReaderAreas ()
		{
			int currentUserId;
			if (_thisUser == null)
			{
				currentUserId = int.MinValue;
			}
			else
			{
				currentUserId = _thisUser.UserId;
			}
			_readerAreasSet = _thisClient.ViewAccessLevels (currentUserId);
			
			if (_readerAreasSet != null)
			{
                _readerAreasList = _readerAreasSet.IndividualReaderAreasDictionary();
				_rdrAreaList = new Dictionary<int, Net2ReaderArea> ();

				foreach (IReaderAreaView readerView in _readerAreasList.Values)
				{
					_rdrAreaList.Add (readerView.AreaId, new Net2ReaderArea (readerView));
				}

				if (_readerAreaSource == null)
				{
					_readerAreaSource = new BindingSource ();
				}
				BuildIndividualAreasGrid ();
				//_readerAreaSource.DataSource = _readerAreasList.Values;
				_readerAreaSource.DataSource = _rdrAreaList.Values;
				readerAreasGridView.DataSource = _readerAreaSource;
			}

			accessLevelComboBox.Enabled = false;
			readerAreasGridView.ReadOnly = false;
			readerAreasGridView.Enabled = true;

            if (readerAreasGridView.Columns.Count > 0)
            {
                readerAreasGridView.Columns[0].Visible = false; // We don't want to display the AreaID info
            }

            readerAreasGridView.Visible = true;
		}

		// Build the Reader Areas grid for manual data binding.
		private void BuildIndividualAreasGrid ()
		{
			readerAreasGridView.AutoGenerateColumns = false;
			readerAreasGridView.Columns.Clear ();
			DataGridViewTextBoxColumn standardColumn;
			DataGridViewComboBoxColumn comboColumn;

			standardColumn = new DataGridViewTextBoxColumn ();
			standardColumn.DataPropertyName = "AreaId";
			standardColumn.ValueType = typeof (int);
			readerAreasGridView.Columns.Add (standardColumn);

			standardColumn = new DataGridViewTextBoxColumn ();
			standardColumn.DataPropertyName = "ReaderAreaName";
			standardColumn.HeaderText = "Reader / Area";
			standardColumn.ValueType = typeof (string);
			readerAreasGridView.Columns.Add (standardColumn);

			comboColumn = new DataGridViewComboBoxColumn ();
			comboColumn.DisplayMember = "DisplayName";
			comboColumn.ValueMember = "ListId";

			Dictionary<int, string> timzonesList = _readerAreasSet.TimezonesDictionary ();
			ListItem nextItem;
			foreach (int timeZoneId in timzonesList.Keys)
			{
				nextItem = new ListItem (timeZoneId, timzonesList[timeZoneId]);
				comboColumn.Items.Add (nextItem);
			}

			comboColumn.DataPropertyName = "TimezoneId";
			comboColumn.HeaderText = "Time Zone";
			comboColumn.ReadOnly = false;
			readerAreasGridView.Columns.Add (comboColumn);
		}

		private void PopulateAccessLevels ()
		{
			_accessLevelsSet = _thisClient.ViewAccessLevels ();
			if (_accessLevelsSet != null)
			{
				_itemsList = _accessLevelsSet.AccessLevelsDictionary ();
				int selectedAccessLevelId = -1;
				if (_thisUser != null)
				{
					selectedAccessLevelId = _thisUser.AccessLevelId;
				}
				PopulateComboList (accessLevelComboBox, selectedAccessLevelId);
				_itemsList.Clear ();
			}
			else
			{
				newUserStatusLabel.Text = _thisClient.LastErrorMessage;
			}

			accessLevelComboBox.Enabled = true;
		//	readerAreasGridView.Enabled = false;
		//	readerAreasGridView.ReadOnly = true;
            readerAreasGridView.Visible = false;
		}

		private void PopulateCustomFields ()
		{
			string derivedControlName = null;
			string derivedFieldName = null;
			string customValue = null;
			TextBox customField = null;
			Type userType;
			PropertyInfo userProp;

			for (int customIdx = 1; customIdx < (CUSTOM_FIELD_COUNT + 1); customIdx++)
			{
				// Derive the control name.
				derivedControlName = string.Format (CultureInfo.InvariantCulture, "field{0}TextBox", customIdx);

				switch (customIdx)
				{
				case 13:
					{
						customField = (TextBox)memoTabPage.Controls[derivedControlName];
						break;
					}
				default:
					{
						customField = (TextBox)customFieldsTabPage.Controls[derivedControlName];
						break;
					}
				}

				// Derive the column name in the DataSet
				derivedFieldName = CustomColumnFromIndex (customIdx);
				userType = _thisUser.GetType ();
				userProp = userType.GetProperty (derivedFieldName);
				customValue = (string)userProp.GetValue (_thisUser, BindingFlags.GetProperty, null, null, CultureInfo.InvariantCulture);

				if (customValue == null)
				{
					customField.Text = "";
				}
				else
				{
					customField.Text = customValue;
				}
			}
		}

        /// <summary>
        /// TODO: Implement when we have verification that the PIN is not in use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void generatePinButton_Click (object sender, EventArgs e)
		{
            int min = 1;
            int max = (int)(Math.Pow(10, double.Parse(txtPinLength.Text)) - 1);

			Random pinGen = new Random ((int)DateTime.Now.Ticks);
			int newPin = pinGen.Next (min, max);
			pinTextBox.Text = newPin.ToString (CultureInfo.CurrentCulture);
		}

		private void cardNumberTextBox_KeyPress (object sender, KeyPressEventArgs e)
		{
			switch (e.KeyChar)
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '\b':
				{
					e.Handled = false;
					break;
				}

			default:
				{
					e.Handled = true;
					break;
				}
			}
		}

		private void addCardButton_Click (object sender, EventArgs e)
		{
			try
			{
				string newCardNumber = cardNumberTextBox.Text;
				int newCardNo = int.Parse (newCardNumber);
				//int newCardType = DeriveSelectedComboValue(cardTypesComboBox);
				int relatedUserId = _thisUser.UserId;

                // TODO: Use proper card types
				bool blnResult = _thisClient.AddCard(newCardNo, 0, relatedUserId);
				if (blnResult)
				{
					cardsComboBox.Items.Add (newCardNo.ToString ());
					cardsComboBox.SelectedIndex = 0;
					cardNumberTextBox.Text = "";
					newUserStatusLabel.Text = "Card added OK";
				}
				else
				{
					newUserStatusLabel.Text = _thisClient.LastErrorMessage;
				}
			}
			catch (Exception excp)
			{
				newUserStatusLabel.Text = excp.Message;
			}
		}

		private void deleteCardButton_Click (object sender, EventArgs e)
		{
			try
			{
				int cardSelectedIdx = cardsComboBox.SelectedIndex;
				if (cardSelectedIdx < 0)
				{
					newUserStatusLabel.Text = "No card selected for deletion.";
					return;
				}

				string cardNumberForDeletion = (string)cardsComboBox.Items[cardSelectedIdx];
				int cardForDeletion = int.Parse (cardNumberForDeletion, CultureInfo.CurrentCulture);
				bool blnResult = _thisClient.DeleteCard (cardForDeletion);
				if (blnResult)
				{
					cardsComboBox.Items.RemoveAt (cardSelectedIdx);
					if (cardsComboBox.Items.Count > 0)
					{
						cardsComboBox.SelectedIndex = 0;
					}
					else
					{
						cardsComboBox.Items.Clear ();
						cardsComboBox.Text = "";
					}
					newUserStatusLabel.Text = "Card deleted OK.";
				}
				else
				{
					newUserStatusLabel.Text = _thisClient.LastErrorMessage;
				}
			}
			catch (Exception excp)
			{
				newUserStatusLabel.Text = excp.Message;
			}
		}

		private void updateUserButton_Click (object sender, EventArgs e)
		{
			// If the user record was passed in through the constructor, we are updating an existing user otherwise we are creating a new one.
			if (_thisUser == null)
			{
				AddUserRecord ();
			}
			else
			{
				SaveUserRecord ();
			}
        }

        private void SaveUserRecord()
        {
			newUserStatusLabel.Text = "Updating...";
			int departmentId = _thisUser.DepartmentId;
			int accessLevelId = _thisUser.AccessLevelId;
			string firstName = _thisUser.FirstName;
			string middleName = _thisUser.MiddleName;
			string surname = _thisUser.Surname;
			string telephone = _thisUser.Telephone;
			string telephoneExt = _thisUser.Extension;
			string faxNo = _thisUser.Fax;
			string pinCode = _thisUser.PIN;
			string pictureFile = _thisUser.Picture;
			DateTime actDate = _thisUser.ActivationDate;
			DateTime expDate = _thisUser.ExpiryDate;

			DateTime newExpiry = (expiresCheckBox.Checked) ? expiryDate.Value : DateTime.MinValue;
			string[] customFields = BuildCustomFieldsArray ();
			int selectedAccessLevel = DeriveSelectedComboValue(accessLevelComboBox);
			int selectedDepartment = DeriveSelectedComboValue(departmentComboBox);

			bool blnResult = false;
			if (accessLevelRadioButton.Checked)
			{
				blnResult = _thisClient.UpdateUserRecord(_thisUser.UserId,
					selectedAccessLevel,
					(departmentId == selectedDepartment) ? -1 : selectedDepartment,
					antiPassbackCheckBox.Checked,
					alarmUserCheckBox.Checked,
					firstNameTextBox.Text.Equals(firstName) ? null : firstNameTextBox.Text,
					middleNameTextBox.Text.Equals(middleName) ? null : middleNameTextBox.Text,
					surnameTextBox.Text.Equals(surname) ? null : surnameTextBox.Text,
					telephoneNoTextBox.Text.Equals(telephone) ? null : telephoneNoTextBox.Text,
					telephoneExtTextBox.Text.Equals(telephoneExt) ? null : telephoneExtTextBox.Text,
					pinTextBox.Text.Equals(pinCode) ? null : pinTextBox.Text,
					pictureFileTextBox.Text.Equals(pictureFile) ? null : pictureFileTextBox.Text,
					activationDate.Value,
					activeCheckBox.Checked,
					faxTextBox.Text.Equals(faxNo) ? null : faxTextBox.Text,
                    newExpiry.Equals(expDate) ? DateTime.MinValue : newExpiry,
					customFields);
			}
			else
			{
				// Updates the DataSet with any changes that may have occurred in the timezones.
				foreach (IndividualReaderAreasSet.IndividualReaderAreasRow indArea in _readerAreasSet.IndividualReaderAreasDataSource.IndividualReaderAreas)
				{
					if (indArea.SelectedTimezoneID != _rdrAreaList[indArea.AreaID].TimezoneId)
					{
						indArea.SelectedTimezoneID = _rdrAreaList[indArea.AreaID].TimezoneId;
					}
				}

				blnResult = _thisClient.UpdateUserRecord (_thisUser.UserId,
					_readerAreasSet,
					(departmentId == selectedDepartment) ? -1 : selectedDepartment,
					antiPassbackCheckBox.Checked,
					alarmUserCheckBox.Checked,
					firstNameTextBox.Text.Equals(firstName) ? null : firstNameTextBox.Text,
					middleNameTextBox.Text.Equals(middleName) ? null : middleNameTextBox.Text,
					surnameTextBox.Text.Equals(surname) ? null : surname,
					telephoneNoTextBox.Text.Equals(telephone) ? null : telephoneNoTextBox.Text,
					telephoneExtTextBox.Text.Equals(telephoneExt) ? null : telephoneExtTextBox.Text,
					pinTextBox.Text.Equals(pinCode) ? null : pinTextBox.Text,
					pictureFileTextBox.Text.Equals(pictureFile) ? null : pictureFileTextBox.Text,
					activationDate.Value,
					activeCheckBox.Checked,
					faxTextBox.Text.Equals(faxNo) ? null : faxTextBox.Text,
					newExpiry.Equals(expDate) ? DateTime.MinValue : newExpiry,
					customFields);

				if (blnResult)
				{
					if (accessLevelId < 100000000)
					{
						accessLevelId = _thisUser.UserId + 100000000;
					}
				}
			}

			if (blnResult)
			{
				newUserStatusLabel.Text = "User updated OK.";
			}
			else
			{
				newUserStatusLabel.Text = _thisClient.LastErrorMessage;
			}
		}

		private void AddUserRecord ()
		{
			int accessLevel = DeriveSelectedComboValue (accessLevelComboBox);
			int departmentId = DeriveSelectedComboValue (departmentComboBox);
			bool antiPassbackInd = antiPassbackCheckBox.Checked;
			bool alarmUser = alarmUserCheckBox.Checked;
			bool activeUser = activeCheckBox.Checked;
			string firstName = (string.IsNullOrEmpty (firstNameTextBox.Text)) ? null : firstNameTextBox.Text;
			string middleName = (string.IsNullOrEmpty (middleNameTextBox.Text)) ? null : middleNameTextBox.Text;
			string surnameString = (string.IsNullOrEmpty (surnameTextBox.Text)) ? null : surnameTextBox.Text;
			string telephoneNo = (string.IsNullOrEmpty (telephoneNoTextBox.Text)) ? null : telephoneNoTextBox.Text;
			string telephoneExt = (string.IsNullOrEmpty (telephoneExtTextBox.Text)) ? null : telephoneExtTextBox.Text;
			string faxNo = (string.IsNullOrEmpty (faxTextBox.Text)) ? null : faxTextBox.Text;
			string pinNumber = (string.IsNullOrEmpty (pinTextBox.Text)) ? null : pinTextBox.Text;
			string pictureFile = (string.IsNullOrEmpty (pictureFileTextBox.Text)) ? null : pictureFileTextBox.Text;
			DateTime activateDate = activationDate.Value;
			string cardNo = cardNumberTextBox.Text;
			int cardNumber = 0;
		    int NewUserId = OemClient.ErrorCodes.AddNewUserFailed;

			if (!string.IsNullOrEmpty (cardNo))
			{
				cardNumber = int.Parse (cardNo, CultureInfo.CurrentCulture);
			}
			int cardTypeID = 0;
			DateTime newExpiryDate = DateTime.MinValue;
			if (expiresCheckBox.Checked)
			{
				if (!expiryDate.Value.Equals (DateTime.Today))
				{
					newExpiryDate = expiryDate.Value;
				}
			}

			string[] customFields = BuildCustomFieldsArray ();

			bool blnResult;
			if (accessLevelRadioButton.Checked)
			{
                NewUserId = _thisClient.AddNewUser (accessLevel, departmentId, antiPassbackInd, alarmUser, firstName, middleName, surnameString,
														telephoneNo, telephoneExt, pinNumber, pictureFile, activateDate, cardNumber, cardTypeID, activeUser,
														faxNo, newExpiryDate, customFields);
			}
			else
			{
				// Updates the DataSet with any changes that may have occurred in the timezones.
				foreach (IndividualReaderAreasSet.IndividualReaderAreasRow indArea in _readerAreasSet.IndividualReaderAreasDataSource.IndividualReaderAreas)
				{
					if (indArea.SelectedTimezoneID != _rdrAreaList[indArea.AreaID].TimezoneId)
					{
						indArea.SelectedTimezoneID = _rdrAreaList[indArea.AreaID].TimezoneId;
					}
				}

                NewUserId = _thisClient.AddNewUser(_readerAreasSet, departmentId, antiPassbackInd, alarmUser, firstName, middleName, surnameString,
														telephoneNo, telephoneExt, pinNumber, pictureFile, activateDate, cardNumber, cardTypeID, activeUser,
														faxNo, newExpiryDate, customFields);
			}

			if (NewUserId != OemClient.ErrorCodes.AddNewUserFailed)
			{
				if (cardNumber > 0)
				{
					cardsComboBox.Items.Add (cardNumber.ToString ());
					cardsComboBox.SelectedIndex = 0;
					cardNumberTextBox.Text = "";
				}
				newUserStatusLabel.Text = "New user was added.";
			}
			else
			{
				newUserStatusLabel.Text = _thisClient.LastErrorMessage;
			}
		}

		private void accessLevelRadioButton_CheckedChanged (object sender, EventArgs e)
		{
			RadioButton thisRadioButton = (RadioButton)sender;
			if (thisRadioButton.Checked)
			{
				PopulateAccessLevels ();
			}
		}

		private void individualRadioButton_CheckedChanged (object sender, EventArgs e)
		{
			RadioButton thisRadioButton = (RadioButton)sender;
			if (thisRadioButton.Checked)
			{
				PopulateIndividualReaderAreas ();
			}
		}

		private string[] BuildCustomFieldsArray()
		{
			string[] customFields = new string[CUSTOM_FIELD_COUNT + 1];
			_customFieldChanges = new Dictionary<int, string> ();
			int entryCount = 0;

			TextBox customText;
			string customName;
			string customValue;
			string customColumn;
			string customDbValue = null;
			Type userType;
			PropertyInfo userProp;

			for (int customIdx = 1; customIdx < customFields.Length; customIdx++)
			{
				customName = string.Format (CultureInfo.InvariantCulture, "field{0}TextBox", customIdx);
				customText = null;
				if (customFieldsTabPage.Controls.ContainsKey (customName))
				{
					customText = (TextBox)customFieldsTabPage.Controls[customName];
				}

				if (memoTabPage.Controls.ContainsKey (customName))
				{
					customText = (TextBox)memoTabPage.Controls[customName];
				}

				customColumn = CustomColumnFromIndex (customIdx);

				if (_thisUser != null)
				{
					userType = _thisUser.GetType ();
					userProp = userType.GetProperty (customColumn);
					customValue = (string)userProp.GetValue (_thisUser, BindingFlags.GetProperty, null, null, CultureInfo.InvariantCulture);
					if (customValue != null)
					{
						customDbValue = customValue;
					}
					else
					{
						customDbValue = "";
					}
				}
				else
				{
					customDbValue = "";
				}

				if (customText != null)
				{
					customValue = customText.Text;
					if (!customValue.Equals(customDbValue))
					{
						customFields[customIdx] = customValue;
						_customFieldChanges.Add (customIdx, customValue);
						entryCount++;
					}
				}

			}

			if (entryCount == 0)
			{
				customFields = null;
				_customFieldChanges = null;
			}

			return customFields;
		}

		private string CustomColumnFromIndex (int customIdx)
		{
			string columnName = null;

			switch (customIdx)
			{
			case 1:
			case 2:
				{
					columnName = string.Format(CultureInfo.InvariantCulture, "Field{0}_100", customIdx);
					break;
				}

			case 13:
				{
					columnName = string.Format(CultureInfo.InvariantCulture, "Field{0}_Memo", customIdx);
					break;
				}

			default:
				{
					columnName = string.Format(CultureInfo.InvariantCulture, "Field{0}_50", customIdx);
					break;
				}
			}

			return columnName;
		}

		private void expiresCheckBox_CheckStateChanged (object sender, EventArgs e)
		{
			CheckBox thisCheck = (CheckBox)sender;
			if (thisCheck.Checked)
			{
				expiryDate.Enabled = true;
			}
			else
			{
				expiryDate.Enabled = false;
			}
		}

		private void btnCancel_Click (object sender, EventArgs e)
		{
			Close ();
		}

        private void txtPinLength_TextChanged(object sender, EventArgs e)
        {
            int value;

            if (int.TryParse(txtPinLength.Text, out value))
            {
                if ((value < 4) || (value > 8))
                {
                    value = 4;
                }
            }
            else
            {
                value = 4;
            }
            txtPinLength.Text = value.ToString();
            pinTextBox.MaxLength = value;
        }
	}
}